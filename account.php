<?php include("includes/header.php");  ?>
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Login/Register</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>                   
          <li class="active">Login/Register</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>Login</h4>
                 <form action="Models/login.php" class="aa-login-form" method="post">
                     <input type="hidden" name="return_url" value="<?php echo "../my-account.php"; ?>"/>
                  <label for="">Username or Email address<span>*</span></label>
                   <input type="text" name="email" required="required" placeholder="Username or email">
                   <label for="">Password<span>*</span></label>
                    <input type="password" name="password" required="required" placeholder="Password">
                    <button type="submit" name="login" class="aa-browse-btn">Login</button>
<!--                    <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
                    <p class="aa-lost-password"><a href="#">Lost your password?</a></p>-->
                  </form>
                </div>
              </div>
              <div class="col-md-6">
                  <?php if(!isset($_SESSION["authenticationCodeCustomer"])){ ?>
                <div class="aa-myaccount-register">                 
                 <h4>Register</h4>
                 <form action="Models/register.php" class="aa-login-form" method="post">
                    <label for="">Full Name<span>*</span></label>
                    <input type="text" name="full_name" required="required" placeholder="Your Full Name"/>
                    
                    <label for="">Username or Email address<span>*</span></label>
                    <input type="text" name="email"  required="required" placeholder="Email">
                    
                    <label for="">Password<span>*</span></label>
                    <input type="password" name="password" required="required" placeholder="Password">
                    
                    <label for="">Phone #</label>
                    <input type="text" name="phone_no" placeholder="Your Phone #"/>
                    
                    <label for="">Shipping Address</label>
                    <textarea name="shipping_address" placeholder="Shipping City"></textarea>
                    
                    <label for="">Shipping City</label>
                    <input type="text" name="shipping_city" required="required" placeholder="Shipping City"/>
                    
                    <label for="">Billing Address</label>
                    <textarea name="billing_address" placeholder="Billing City"></textarea>
                    
                    <label for="">Billing City</label>
                    <input type="text" name="billing_city" required="required" placeholder="Billing City"/>
                    
                    <button type="submit" name="register" class="aa-browse-btn">Register</button>                    
                  </form>
                </div>
                  <?php } ?>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

  <?php include("includes/footer.php"); ?>
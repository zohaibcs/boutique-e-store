<?php include("includes/header.php");  ?>
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Cart Page</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>                   
          <li class="active">Cart</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table">
               
                    <form method="post" action="update-cart.php">
                        <input type="hidden" value="update" name="type" />
                        <div class="table-responsive">
                    <table width="100%" class="table" cellpadding="6" cellspacing="0">
                     <thead>
                      <tr>
                        <th>Remove</th>
                        <th>Quantity</th>
                        <th>Product</th>
                        <th>Product ID </th>
                        <th>Price</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    
                      <tbody>
                        <?php
                        if(isset($_SESSION["cart_products"])) //check session var
                        {
                            $currency = "Rs";
                            $shipping_cost = 0;
                            $total = 0; //set initial total value
                            $b = 0; //var for zebra stripe table 
                            $taxes = [0=>0];
                            foreach ($_SESSION["cart_products"] as $cart_itm)
                            {
                                //set variables to use in content below
                                $product_image = $cart_itm["product_image"];
                                $product_name = $cart_itm["product_name"];
                                $product_qty = $cart_itm["product_qty"];
                                $product_price = $cart_itm["product_price"];
                                $product_code = $cart_itm["product_id"];
                                $product_color = $cart_itm["product_color"];
                                $subtotal = ($product_price * $product_qty); //calculate Price x Qty

                                $bg_color = ($b++%2==1) ? 'odd' : 'even'; //class for zebra stripe 
                                echo '<tr class="'.$bg_color.'">';
                                echo '<td><input type="checkbox" name="remove_code[]" value="'.$product_code.'" /><fa class="fa fa-close"></fa></td>';
                                echo '<td><input type="text" size="2" maxlength="2" name="product_qty['.$product_code.']" value="'.$product_qty.'" /></td>';
                                echo '<td><img src="'.UPLOAD_PATH_DISPLAY.$product_image.'" /></td>';
                                echo '<td>'.$product_name.'</td>';
                                echo '<td>'.$currency.$product_price.'</td>';
                                echo '<td>'.$currency.$subtotal.'</td>';
                                echo '</tr>';
                                $total = ($total + $subtotal); //add subtotal to total var
                            }

                            $grand_total = $total + $shipping_cost; //grand total including shipping cost
                            foreach($taxes as $key => $value){ //list and calculate all taxes in array
                                    $tax_amount     = round($total * ($value / 100));
                                    $tax_item[$key] = $tax_amount;
                                    $grand_total    = $grand_total + $tax_amount;  //add tax val to grand total
                            }

                            $list_tax       = '';
                            foreach($tax_item as $key => $value){ //List all taxes
                                $list_tax .= $key. ' : '. $currency. sprintf("%01.2f", $value).'<br />';
                            }
                            $shipping_cost = ($shipping_cost)?'Shipping Cost : '.$currency. sprintf("%01.2f", $shipping_cost).'<br />':'';
                        }
                        ?>
                        <tr><td colspan="6"><span style="float:right;text-align: right;"><?php echo $shipping_cost. $list_tax; ?>Amount Payable : <?php echo sprintf("%01.2f", $grand_total);?></span></td></tr>
                        <tr>
                        <td colspan="6" class="aa-cart-view-bottom">
                            <a href="index.php" class="button">Add More Items</a>
                          <input class="aa-cart-view-btn" type="submit" value="Update Cart">
                        </td>
                      </tr>
                      </tbody>
                    </table>
                    <input type="hidden" name="return_url" value="<?php  echo $current_url; ?>" />
                        </div>
                    </form>
<!--
             <form action="">
               <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><a class="remove" href="#"><fa class="fa fa-close"></fa></a></td>
                        <td><a href="#"><img src="img/man/polo-shirt-1.png" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">Polo T-Shirt</a></td>
                        <td>Rs.2500</td>
                        <td><input class="aa-cart-quantity" type="number" value="1"></td>
                        <td>Rs.2500</td>
                      </tr>
                      <tr>
                        <td><a class="remove" href="#"><fa class="fa fa-close"></fa></a></td>
                        <td><a href="#"><img src="img/man/polo-shirt-2.png" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">Polo T-Shirt</a></td>
                        <td>Rs.1500</td>
                        <td><input class="aa-cart-quantity" type="number" value="1"></td>
                        <td>Rs.1500</td>
                      </tr>
                      <tr>
                        <td><a class="remove" href="#"><fa class="fa fa-close"></fa></a></td>
                        <td><a href="#"><img src="img/man/polo-shirt-3.png" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">Polo T-Shirt</a></td>
                        <td>Rs.500</td>
                        <td><input class="aa-cart-quantity" type="number" value="1"></td>
                        <td>Rs.500</td>
                      </tr>
                      <tr>
                        <td colspan="6" class="aa-cart-view-bottom">
                         
                          <input class="aa-cart-view-btn" type="submit" value="Update Cart">
                        </td>
                      </tr>
                      </tbody>
                  </table>
                </div>
             </form>-->
             <!-- Cart Total view -->
             <div class="cart-view-total">
               <h4>Cart Totals</h4>
               <table class="aa-totals-table">
                 <tbody>
                   <tr>
                     <th>Subtotal</th>
                     <td>Rs.<?php echo $grand_total; ?></td>
                   </tr>
                   <tr>
                     <th>Total</th>
                     <td>Rs.<?php echo $grand_total; ?></td>
                   </tr>
                 </tbody>
               </table>
               <a href="checkout.php" class="aa-cart-view-btn">Proceed to Checkout</a>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->


  <?php include("includes/footer.php"); ?>
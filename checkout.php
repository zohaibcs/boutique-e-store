<?php include("includes/header.php");  ?>
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Checkout Page</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>                   
          <li class="active">Checkout</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="checkout">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="checkout-area">
            <div class="row">
              <div class="col-md-8">
                  <?php if(!empty($_GET["msg"])){ ?>
                  <div style="color: red;"><?php echo $_GET["msg"];?></div>
                  <?php } ?>
                <div class="checkout-left">
                  <div class="panel-group" id="accordion">
                    <!-- Login section -->
                    <?php if(!isset($_SESSION["authenticationCodeCustomer"])){ ?>
                    <div class="panel panel-default aa-checkout-login">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Client Login 
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <form action="Models/login.php" method="post">
                            <input type="hidden" name="return_url" value="../checkout.php"/>
                        <div class="panel-body">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat voluptatibus modi pariatur qui reprehenderit asperiores fugiat deleniti praesentium enim incidunt.</p>
                          <input type="text" name="email" required="required" placeholder="Email">
                          <input type="password" name="password" required="required" placeholder="Password">
                          <button type="submit" name="login" class="aa-browse-btn">Login</button>
                          <!--<label for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>-->
                          <!--<p class="aa-lost-password"><a href="#">Lost your password?</a></p>-->
                        </div>
                        </form>

                      </div>
                    </div>
                    <?php } ?>
                    <!-- Billing Details -->
<!--                    <div class="panel panel-default aa-checkout-billaddress">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Billing Details
                          </a>
                        </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input type="text" placeholder="First Name*">
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input type="text" placeholder="Last Name*">
                              </div>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                                <input type="text" placeholder="Company name">
                              </div>                             
                            </div>                            
                          </div>  
                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input type="email" placeholder="Email Address*">
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input type="tel" placeholder="Phone*">
                              </div>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                                <textarea cols="8" rows="3">Address*</textarea>
                              </div>                             
                            </div>                            
                          </div>   
                          
                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input type="text" placeholder="Appartment, Suite etc.">
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input type="text" placeholder="City / Town*">
                              </div>
                            </div>
                          </div>                                     
                        </div>
                      </div>
                    </div>-->
                    <!-- Shipping Address -->
                    <?php
                    if(isset($_SESSION["id"]) && $_SESSION["id"] > 0){
                        $shippingQry = mysqli_query($con, "SELECT * FROM customers WHERE id = '".$_SESSION["id"]."'");
                        $sr = mysqli_fetch_object($shippingQry);
                    }
                    ?>
                    <form action="Models/place-order.php" method="post" id="place-order-form">
                            <div class="panel panel-default aa-checkout-billaddress">
                              <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                    Address
                                  </a>
                                </h4>
                              </div>
                              <div id="collapseFour" class="panel-collapse collapse in">
                                <div class="panel-body">
                                 <div class="row">
                                    <div class="col-md-6">
                                      <div class="aa-checkout-single-bill">
                                          <input type="text" name="full_name" required="required" value="<?php echo (!empty($sr))?$sr->full_name : ''; ?>" placeholder="Full Name*">
                                      
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="aa-checkout-single-bill">
                                          <input type="tel" placeholder="Phone*" required="required" name="phone_no" value="<?php echo (!empty($sr))?$sr->full_name : ''; ?>">
                                      </div>
                                    </div>
                                  </div> 
                                    <?php if(!isset($_SESSION["authenticationCodeCustomer"])){ ?>
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="aa-checkout-single-bill">
                                              <input type="email" name="email" required="required" value="<?php echo (!empty($sr))?$sr->email : ''; ?>" placeholder="Email Address*">
                                            </div>                             
                                          </div>
                                          <div class="col-md-6">
                                            <div class="aa-checkout-single-bill">
                                                <input type="password" placeholder="Password*" required="required" name="password" value="<?php echo (!empty($sr))?$sr->full_name : ''; ?>">
                                            </div>
                                          </div>
                                        </div>
                                    <?php } ?>
                                    <fieldset>
                                        <legend>Shipping Address</legend>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="aa-checkout-single-bill">
                                          <textarea cols="8" rows="3" required="required" name="shipping_address"><?php echo (!empty($sr))? $sr->shipping_address : ''; ?></textarea>
                                        </div>                             
                                      </div>                            
                                    </div>   
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="aa-checkout-single-bill">
                                          <input type="text" placeholder="City / Town*" required="required" name="shipping_city" value="<?php echo (!empty($sr))?$sr->shipping_city : ''; ?>">
                                        </div>
                                      </div>
                                    </div>
                                    </fieldset>
                                    
                                    <fieldset>
                                        <legend>Billing Address</legend>
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="aa-checkout-single-bill">
                                              <textarea cols="8" rows="3" name="billing_address"><?php echo (!empty($sr))? $sr->billing_address : ''; ?></textarea>
                                            </div>                             
                                          </div>                            
                                        </div>   
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="aa-checkout-single-bill">
                                              <input type="text" placeholder="City / Town*" name="billing_city" value="<?php echo (!empty($sr))?$sr->billing_city : ''; ?>">
                                            </div>
                                          </div>
                                        </div>
                                    </fieldset>
                                        
                                   <div class="row">
                                    <div class="col-md-12">
                                      <div class="aa-checkout-single-bill">
                                        <textarea cols="8" rows="3" name="special_note">Special Notes</textarea>
                                      </div>                             
                                    </div>                            
                                  </div>              
                                </div>
                              </div>
                            </div>
                        </form>

                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkout-right">
                  <h4>Order Summary</h4>
                  <div class="aa-order-summary-area">
                      <table width="100%" class="table table-responsive" cellpadding="6" cellspacing="0">
                     <thead>
                      <tr>
                        <th>Product</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    
                      <tbody>
                        <?php
                        if(isset($_SESSION["cart_products"])) //check session var
                        {
                            $currency = "Rs";
                            $shipping_cost = 0;
                            $total = 0; //set initial total value
                            $b = 0; //var for zebra stripe table 
                            $taxes = [0=>0];
                            foreach ($_SESSION["cart_products"] as $cart_itm)
                            {
                                //set variables to use in content below
                                $product_image = $cart_itm["product_image"];
                                $product_name = $cart_itm["product_name"];
                                $product_qty = $cart_itm["product_qty"];
                                $product_price = $cart_itm["product_price"];
                                $product_color = $cart_itm["product_color"];
                                $subtotal = ($product_price * $product_qty); //calculate Price x Qty

                                echo '<tr >';
                                echo '<td>'.$product_name.'<strong> x '.$product_qty.'</strong></td>';
                                echo '<td>'.$currency.$product_price.'</td>';
                                echo '</tr>';
                                $total = ($total + $subtotal); //add subtotal to total var
                            }

                            $grand_total = $total + $shipping_cost; //grand total including shipping cost
                            foreach($taxes as $key => $value){ //list and calculate all taxes in array
                                    $tax_amount     = round($total * ($value / 100));
                                    $tax_item[$key] = $tax_amount;
                                    $grand_total    = $grand_total + $tax_amount;  //add tax val to grand total
                            }

                            $list_tax       = '';
                            foreach($tax_item as $key => $value){ //List all taxes
                                $list_tax .= $key. ' : '. $currency. sprintf("%01.2f", $value).'<br />';
                            }
                        }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Subtotal</th>
                          <td>Rs.<?php echo $grand_total; ?></td>
                        </tr>
                         <tr>
                          <th>Tax</th>
                          <td>Rs.0</td>
                        </tr>
                         <tr>
                          <th>Total</th>
                          <td>Rs.<?php echo $grand_total; ?></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="aa-payment-method">                    
                    <input type="submit" value="Place Order" onclick="PlaceOrder()" class="aa-browse-btn">                
                  </div>
                </div>
              </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->
<script>
    function PlaceOrder(){ 
        $("#place-order-form").submit();
    }
</script>
  <?php include("includes/footer.php"); ?>
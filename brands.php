<?php include("includes/header.php");  ?>

  <!-- Products section -->

  <!-- / products section -->

<!-- start prduct navigation -->
                 <ul class="nav nav-tabs aa-products-tab">
                   
                  
                  </ul>
                 

<div class="container">
    <div class="row">

      <?php $bqry = mysqli_query($con, "SELECT * FROM boutique WHERE status = 'Approved'"); ?>
      <?php while($br = mysqli_fetch_object($bqry)){ ?>
        <?php if(!empty($br->logo)){ ?>
    <!-- TH1 -->
        <div class="col-sm-4">
            <div class="thumbnail">
              <a href="product.php?brand=<?php echo $br->full_name; ?>" class="">
                <img src="<?php echo UPLOAD_PATH_DISPLAY.$br->logo; ?>" alt="..." class="">
               </a>
            </div>
        </div>
      <?php } ?>
      <?php } ?>
      </div> <!-- end container -->
      </div> <!-- end container -->


 <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12x" >
            <ul class="pager" >
              <li>
                <a href="#">←  Prev </a>
              </li>
              <li>
                <a href="#">Next  →</a>
              </li>

                  </ul>
          </div>
        </div>
      </div>
    </div>


   <!-- Support section -->
  <section id="aa-support">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-support-area">
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-truck"></span>
                <h4>FREE SHIPPING</h4>
                <P>Orders are packed and shipped Monday-Friday only. Most orders are shipped within 24 hours from the order date. Orders placed on the weekend and select holidays are processed on the next business day.
</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-clock-o"></span>
                <h4>30 DAYS MONEY BACK</h4>
                <P>Online purchases made through boutique-estore.com are valid for exchange, credit, or refund within 30 days from the ship date. Your return form and invoice provide a postmark date that specifies the last day you have to return your merchandise by mail. Refunds are only available for online purchases returned by mail. 
</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-phone"></span>
                <h4>SUPPORT 24/7</h4>
                <P>Our Online Customer Agent is always there to assist you.</P>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Support section -->

  <?php include("includes/footer.php"); ?>
-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2017 at 09:06 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boutique_e_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `boutique`
--

CREATE TABLE `boutique` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `slug` varchar(70) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `phone_no` varchar(45) DEFAULT NULL,
  `address` text,
  `city` varchar(45) DEFAULT NULL,
  `introduction` text,
  `status` varchar(45) DEFAULT 'Not Approved',
  `active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boutique`
--

INSERT INTO `boutique` (`id`, `full_name`, `slug`, `logo`, `email`, `password`, `phone_no`, `address`, `city`, `introduction`, `status`, `active`, `created_at`, `updated_at`) VALUES
(11, 'Khaadi', 'khaadi', '1483718508_khaadilogo350x250.jpg', 'khaadi@gmail.com', 'khaadi123', '3134242427', 'Gulberg', 'Lahore', '', 'Approved', NULL, NULL, NULL),
(12, 'Gul Ahmed', 'gul-ahmed', '1483718606_images350x250.jpg', 'gulahmed@gmail.com', 'gulahmed123', '3214532111', 'Gulberg', 'Lahore', '', 'Approved', NULL, NULL, NULL),
(13, 'Origins', 'origins', '1483787259_Origins350x250.jpg', 'origins@yahoo.com', 'origins123', '04236451238', 'Gulberg', 'Lahore', '', 'Approved', NULL, NULL, NULL),
(14, 'Kayseria', 'kayseria', '1483787420_Kayseria350x250.png', 'kayseria@gmail.com', 'kayseria123', '04237925814', 'DHA, karachi', 'Karachi', '', 'Approved', NULL, NULL, NULL),
(15, 'LimeLight', 'limelight', '1483787560_limelight350x250.jpg', 'limelight@gmail.com', 'limelight123', '04237931482', 'MM Alam', 'Lahore', '', 'Approved', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Stitched', 'stitched', '', NULL, NULL, NULL),
(2, 'Unstiched', 'unstiched', '', NULL, NULL, NULL),
(3, 'New Arrivals', 'new-arrivals', '', NULL, NULL, NULL),
(4, 'Sale', 'sale', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone_no` varchar(45) DEFAULT NULL,
  `shipping_address` text,
  `shipping_city` varchar(45) DEFAULT NULL,
  `billing_address` text,
  `billing_city` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `full_name`, `email`, `username`, `password`, `phone_no`, `shipping_address`, `shipping_city`, `billing_address`, `billing_city`, `created_at`, `update_at`) VALUES
(1, 'Zohaib', 'zohaib@gmail.com', NULL, 'password123', '0301 797 6893', 'lkkj', 'kjhjkhjkh', 'jkhjkkjk', 'kjhjkhj', NULL, NULL),
(2, 'Rabnawaz Bhatti', 'bhattijan01@gmail.com', NULL, 'ashes', 'Rabnawaz Bhatti', 'phase 4, DHA', 'Lahore', 'phase 4, DHA', 'Lahore', NULL, NULL),
(3, 'Rana Mujahid', 'ranamujahid01@gmail.com', NULL, 'mujahid', '3224244589', 'Phase 4, DHA', 'Lahore', 'Phase4, DHA', 'Lahore', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_details`
--

CREATE TABLE `login_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `last_login_date_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_details`
--

INSERT INTO `login_details` (`id`, `member_id`, `member_type`, `email`, `password`, `active`, `last_login_date_time`) VALUES
(4, 1, 'admin', 'arslaanmalik@yahoo.com', 'password123', 1, NULL),
(6, 1, 'customer', 'zohaib@gmail.com', 'password123', 1, NULL),
(7, 2, 'boutique', 'beechtree@gmail.com', 'beechtree123', 1, NULL),
(8, 3, 'boutique', 'elan@gmail.com', 'elan123', 1, NULL),
(9, 4, 'boutique', 'lala@gmail.com', 'lala123', 1, NULL),
(10, 5, 'boutique', 'crescent@gmail.com', 'crescent123', 1, NULL),
(11, 2, 'customer', 'bhattijan01@gmail.com', 'ashes', 1, NULL),
(15, 9, 'boutique', 'arslaanmalik@yahoo.com', 'password123', 1, NULL),
(16, 10, 'boutique', 'khaadi@yahoo.com', 'khaadi123', 1, NULL),
(17, 11, 'boutique', 'khaadi@gmail.com', 'khaadi123', 1, NULL),
(18, 12, 'boutique', 'gulahmed@gmail.com', 'gulahmed123', 1, NULL),
(19, 13, 'boutique', 'origins@yahoo.com', 'origins123', 1, NULL),
(20, 14, 'boutique', 'kayseria@gmail.com', 'kayseria123', 1, NULL),
(21, 15, 'boutique', 'limelight@gmail.com', 'limelight123', 1, NULL),
(22, 3, 'customer', 'ranamujahid01@gmail.com', 'mujahid', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ordered_products`
--

CREATE TABLE `ordered_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordered_products`
--

INSERT INTO `ordered_products` (`id`, `order_id`, `product_id`, `brand_id`, `quantity`, `size`, `color`, `status`, `created_at`, `updated_at`) VALUES
(3, 17, 29, 13, 2, 'S', '', 'Pending', NULL, NULL),
(4, 17, 32, 13, 1, '', '', 'Pending', NULL, NULL),
(5, 18, 15, 11, 1, '', '', 'Processing', NULL, NULL),
(6, 18, 11, 11, 1, '', '', 'Pending', NULL, NULL),
(7, 18, 10, 11, 1, '', '', 'Pending', NULL, NULL),
(8, 18, 12, 11, 1, '', '', 'Pending', NULL, NULL),
(9, 19, 6, 12, 1, '', '', 'Pending', NULL, NULL),
(10, 20, 10, 11, 1, '', '', 'Pending', NULL, NULL),
(11, 21, 10, 11, 1, '', '', 'Pending', NULL, NULL),
(12, 21, 37, 14, 1, '', '', 'Pending', NULL, NULL),
(13, 21, 36, 14, 1, '', '', 'Pending', NULL, NULL),
(14, 21, 38, 14, 1, '', '', 'Pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `special_note` text,
  `status` varchar(45) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `special_note`, `status`, `total`, `created_at`, `updated_at`) VALUES
(17, 2, 'Special Notes', 'Pending', '0', '2017-01-06 19:00:00', NULL),
(18, 2, 'Special Notes', 'Pending', '0', '2017-01-06 19:00:00', NULL),
(19, 2, 'Special Notes', 'Pending', '0', '2017-01-06 19:00:00', NULL),
(20, 2, 'Special Notes', 'Shipped', '0', '2017-01-06 19:00:00', NULL),
(21, 2, 'Special Notes', 'Delivered', '0', '2017-01-06 19:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `quantity` varchar(45) DEFAULT NULL,
  `remaining_quantity` int(11) DEFAULT NULL,
  `sku_code` varchar(45) DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `boutique_id` int(10) UNSIGNED NOT NULL,
  `description` text,
  `active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `price`, `size`, `color`, `quantity`, `remaining_quantity`, `sku_code`, `category_id`, `boutique_id`, `description`, `active`, `created_at`, `updated_at`) VALUES
(5, 'BMS', NULL, '1500', 'S,M,L,XL,XXL', 'Grey,White', '10,15,12,20,5', NULL, 'BMS11', 1, 12, '', 1, NULL, NULL),
(6, 'BMB', NULL, '2000', 'S,M', 'Green,Brown', '10,12', NULL, 'BMB1', 1, 12, '', 1, NULL, NULL),
(7, 'SMA', NULL, '1200', 'S,M,L', 'White,Black,Grey', '12,17,2', NULL, 'SMA10', 2, 12, '', 1, NULL, NULL),
(8, 'SMA01', NULL, '1500', 'S,L', 'Purple', '2,6', NULL, 'SMA101', 2, 12, '', 1, NULL, NULL),
(9, 'SAS', NULL, '2500', 'S,M,L', 'White,Grey,Red,Black', '17,15,12', 0, 'SAS112', 3, 12, '', 1, NULL, NULL),
(10, 'A1A', NULL, '1200', 'S,M,L', 'Orange,White', '12,13,18', 3, 'A1AS', 3, 11, '', 1, NULL, NULL),
(11, 'A1B', NULL, '1800', 'S,L', 'Grey,Green', '10,10', NULL, 'A1B01', 3, 11, '', 1, NULL, NULL),
(12, 'SAL', NULL, '800', 'S,XL', 'Black,White,', '3,10', NULL, 'SAL1', 4, 11, '', 1, NULL, NULL),
(13, 'SAL2', NULL, '900', 'S,L', 'Blue,Green', '6,2', NULL, 'SAL2', 4, 11, '', 1, NULL, NULL),
(14, 'SAL3', NULL, '1300', 'S,M', 'Red,Blue', '3,5', NULL, 'SAL03', 4, 11, '', 1, NULL, NULL),
(15, 'STI', NULL, '2200', 'S,M,L,XL', 'Green,Red', '12,10,15,5', NULL, 'STI10', 1, 11, 'Test', 1, NULL, NULL),
(16, 'STI02', NULL, '2500', 'S,M,L,XL', 'Grey', '10,10,10,10', NULL, 'STI002', 1, 11, '', 1, NULL, NULL),
(17, 'UNS1', NULL, '1800', 'S,M,L', 'Yellow', '10,15,20', NULL, 'UNS01', 2, 11, '', 1, NULL, NULL),
(18, 'UNS2', NULL, '1600', 'S,M,L', 'Red,Pink,Blue', '12,11,13', NULL, 'UNS02', 2, 11, '', 1, NULL, NULL),
(19, 'EMB1', NULL, '3500', 'S,M,L', 'White,Black,Blue', '18,20,13', NULL, 'EMB01', 3, 15, '', 1, NULL, NULL),
(20, 'EMB2', NULL, '3000', 'S,M,L', 'Pink,Purple', '20,20,20', NULL, 'EMB02', 3, 15, '', 1, NULL, NULL),
(22, 'SAL1', NULL, '1200', 'S,L', 'Pink', '3,6', NULL, 'SAL01', 4, 15, '', 1, NULL, NULL),
(23, 'SAL2', NULL, '1300', 'S', 'Green', '5', NULL, 'SAL002', 4, 15, '', 1, NULL, NULL),
(24, 'STC01', NULL, '1600', 'M,XL', 'Red,Black', '4,7', NULL, 'STC001', 1, 15, '', 1, NULL, NULL),
(25, 'STC02', NULL, '1600', 'S,M,L', 'White,Red,White', '15,12,12', NULL, 'STC002', 1, 15, '', 1, NULL, NULL),
(26, 'UNL1', NULL, '1800', 'S,M,L', 'Grey,Black', '10,10,12', NULL, 'UNL01', 2, 15, '', 1, NULL, NULL),
(27, 'UNL2', NULL, '2100', 'S,M,L', 'Pink,White', '15,15,15', NULL, 'UNL02', 2, 15, '', 1, NULL, NULL),
(28, 'NAO1', NULL, '2500', 'S,M,L', 'White', '13,15,15', NULL, 'NAO01', 3, 13, '', 1, NULL, NULL),
(29, 'NA02', NULL, '2200', 'S,M,L', 'Red,Black,Blue', '12,12,12', NULL, 'NAO02', 3, 13, '', 1, NULL, NULL),
(30, 'SAO1', NULL, '1200', 'S', 'Grey', '3', NULL, 'SAO02', 4, 13, '', 1, NULL, NULL),
(31, 'SL02', NULL, '1400', 'M', 'White,Green,Red', '3', NULL, 'SLO02', 4, 13, '', 1, NULL, NULL),
(32, 'STO1', NULL, '2500', 'S,M,L,XL,XXL', 'Grey', '18,13,15,7,2', NULL, 'STO01', 1, 13, '', 1, NULL, NULL),
(33, 'STO2', NULL, '2500', 'S,M', 'Red', '15,15', NULL, 'STO02', 1, 13, '', 1, NULL, NULL),
(34, 'UNO1', NULL, '1800', 'S,L', 'Black', '10,10', NULL, 'UNO01', 2, 13, '', 1, NULL, NULL),
(35, 'UNO2', NULL, '1800', 'S,M,L', 'Purple', '12,12,9', NULL, 'UNO02', 2, 13, '', 1, NULL, NULL),
(36, 'NAK1', NULL, '2200', 'S,M,L', 'White,Black,', '18,20,15', NULL, 'NAK01', 3, 14, '', 1, NULL, NULL),
(37, 'NAK2', NULL, '2200', 'S,M', 'White', '10,10', NULL, 'NAK02', 3, 14, '', 1, NULL, NULL),
(38, 'SAK1', NULL, '1600', 'S,L', 'Pink', '2,5', NULL, 'SAK01', 4, 14, '', 1, NULL, NULL),
(39, 'SAK2', NULL, '1600', 'S,L,XL', 'Pink', '3,8,2', NULL, 'SLK02', 4, 14, '', 1, NULL, NULL),
(40, 'STK1', NULL, '2800', 'S,M,L,XL,XXL', 'Pink', '5,5,5,5,5', NULL, 'STK01', 1, 14, '', 1, NULL, NULL),
(41, 'STK2', NULL, '2600', 'S,M,L', 'Black,Dark Blue', '5,8,4', NULL, 'STK02', 1, 14, '', 1, NULL, NULL),
(42, 'UNK2', NULL, '2000', 'S,M,L', 'White', '12,12,12', NULL, 'UNK02', 2, 14, '', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_name` varchar(200) DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `image_name`, `product_id`, `created_at`, `updated_at`) VALUES
(3, '1483718823_CD5A3792759007fa74861bc6c15d2f6563cdagrande250x300.jpg', 5, NULL, NULL),
(4, '1483718947_K1Bgrande250x300.jpg', 6, NULL, NULL),
(5, '1483719172_K3Bgrande250x300.jpg', 7, NULL, NULL),
(6, '1483719305_TCD3B1grande250x300.jpg', 8, NULL, NULL),
(7, '1483719403_BMS13250x300.jpg', 9, NULL, NULL),
(8, '1483786104_a16707ai250x300.jpg', 10, NULL, NULL),
(9, '1483786193_a16709ai250x300.jpg', 11, NULL, NULL),
(10, '1483786289_b16710ai250x300.jpg', 12, NULL, NULL),
(11, '1483786374_b16801i250x300.jpg', 13, NULL, NULL),
(12, '1483786505_b16802i250x300.jpg', 14, NULL, NULL),
(14, '1483786676_o16806i250x300.jpg', 16, NULL, NULL),
(15, '1483786785_b16804bi250x300.jpg', 17, NULL, NULL),
(16, '1483786908_e16704bi250x300.jpg', 18, NULL, NULL),
(17, '1483787685_embroidedshortkurti250x300.jpg', 19, NULL, NULL),
(18, '1483787801_embroideredlongshirt250x300 (1).jpg', 20, NULL, NULL),
(21, '1483793234_embroideredlongshirt-1250x300.jpg', 22, NULL, NULL),
(22, '1483793321_embroidered-short-shirt (2).jpg', 23, NULL, NULL),
(23, '1483793418_embroideredshortshirt250x300.jpg', 24, NULL, NULL),
(24, '1483793668_embroideredshortshirt-1250x300.jpg', 25, NULL, NULL),
(25, '1483793864_embroideredshortshirt-2250x300.jpg', 26, NULL, NULL),
(26, '1483793926_embroideredshortshirt-3250x300.jpg', 27, NULL, NULL),
(27, '1483794200_16w100250x300.jpg', 29, NULL, NULL),
(28, '1483794213_16w98250x300.jpg', 28, NULL, NULL),
(29, '1483794312_digitalembroidedshirt250x300.jpg', 30, NULL, NULL),
(30, '1483794391_digitalprintedshirt250x300.jpg', 31, NULL, NULL),
(31, '1483794506_16w47250x300.jpg', 32, NULL, NULL),
(32, '1483794631_16w51250x300.jpg', 33, NULL, NULL),
(33, '1483794802_16w59250x300.jpg', 34, NULL, NULL),
(34, '1483794871_16w82250x300.jpg', 35, NULL, NULL),
(35, '1483795436_kays333722321250x300.jpg', 36, NULL, NULL),
(36, '1483795477_q62a03932250x300.jpg', 37, NULL, NULL),
(37, '1483795532_q62a8946250x300.jpg', 38, NULL, NULL),
(38, '1483795578_q62a970121250x300.jpg', 39, NULL, NULL),
(39, '1483795644_q62a06692021250x300.jpg', 40, NULL, NULL),
(40, '1483795704_q62a11181976250x300.jpg', 41, NULL, NULL),
(41, '1483795891_q62a055519891250x300.jpg', 42, NULL, NULL),
(45, '1485373509_Kayseria-Prints-2014-Kayseria-Summer-Spring-Lawn-Suits-Prints-2014-Huma-Khan-Model-Dresses-2.jpg', 15, NULL, NULL),
(46, '1485373509_Lawn-Suits1.jpg', 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `size_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`product_id`, `size_id`, `quantity`, `created_at`, `updated_at`) VALUES
(15, 1, 3, NULL, NULL),
(15, 2, 4, NULL, NULL),
(15, 4, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `short_name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `short_name`, `created_at`, `updated_at`) VALUES
(1, 'Small', 'S', NULL, NULL),
(2, 'Medium', 'M', NULL, NULL),
(3, 'Large', 'L', NULL, NULL),
(4, 'Extra Large', 'XL', NULL, NULL),
(5, 'Double XL', 'XXL', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE `super_admin` (
  `id` int(11) UNSIGNED NOT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`id`, `full_name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Arsalan malik', 'arslaanmalik@yahoo.com', 'password123', '2016-12-19 19:00:00', '2016-12-19 19:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boutique`
--
ALTER TABLE `boutique`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `login_details`
--
ALTER TABLE `login_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_ordered_products_order_id_idx` (`order_id`),
  ADD KEY `fk_ordered_products_products1_idx` (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_order_id_customers1_idx` (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_products_categeries1_idx` (`category_id`),
  ADD KEY `fk_products_boutique1_idx` (`boutique_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_product_images_products1_idx` (`product_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`product_id`,`size_id`),
  ADD KEY `fk_product_sizes_sizes1_idx` (`size_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boutique`
--
ALTER TABLE `boutique`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_details`
--
ALTER TABLE `login_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ordered_products`
--
ALTER TABLE `ordered_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `super_admin`
--
ALTER TABLE `super_admin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD CONSTRAINT `fk_ordered_products_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ordered_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_order_id_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_boutique1` FOREIGN KEY (`boutique_id`) REFERENCES `boutique` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_categeries1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `fk_product_images_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD CONSTRAINT `fk_product_sizes_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_sizes_sizes1` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

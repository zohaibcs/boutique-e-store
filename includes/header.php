<?php 
session_start();
include("admin/includes/configuration.php"); 
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Boutique E-Store| Home</title>
    
    <!-- Font awesome -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->

    <!-- Main style sheet -->
    <link href="css/style.css" rel="stylesheet" type='text/css'/>    
    <link href="css/custom.css" rel="stylesheet" type='text/css'/>    

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
        <style>
            a.aa-add-card-btn:hover{
                cursor: pointer;
            }
            
            a.dont-add{
                cursor: text;
            }
            
            .aa-product-img img{
                width: 100%;
            }
            .NotifyMsg{
                padding: 10px;
                text-align: center;
                background: blue;
                color: #FFF;
            }
        </style>
  </head>
  <body> 
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

<?php if(!empty($_SESSION["msg"])){ ?>
  <div class="NotifyMsg">
      <?php echo $_SESSION["msg"]; ?>
  </div>
<?php $_SESSION["msg"] = "";} ?>
  <!-- Start header section -->
  <header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-top-area">
              <!-- start header top left -->
              <div class="aa-header-top-left">
                <!-- start language -->
<!--                <div class="aa-language">
                  <div class="dropdown">
                    <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <img src="img/flag/english.jpg" alt="english flag">ENGLISH
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#"><img src="img/flag/french.jpg" alt="">FRENCH</a></li>
                      <li><a href="#"><img src="img/flag/english.jpg" alt="">ENGLISH</a></li>
                    </ul>
                  </div>
                </div>-->
                <!-- / language -->

                <!-- start currency -->
                <div class="aa-currency">
                  <div class="dropdown">
                    <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i class="fa fa-rupee"></i>PKR
                      <span class="caret"></span>
                    </a>
<!--                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#"><i class="fa fa-usd"></i>USD</a></li>
                      <li><a href="#"><i class="fa fa-euro"></i>EURO</a></li>
                      <li><a href="#"><i class="fa fa-jpy"></i>YEN</a></li>
                    
                    </ul>-->
                  </div>
                </div>
                <!-- / currency -->
                <!-- start cellphone -->
                <div class="cellphone hidden-xs">
                  <p><span class="fa fa-phone"></span>0423-5749286</p> <p><span class="fa fa-phone"></span>0300-4268562</p>
                   
                </div>
                <!-- / cellphone -->
                
              </div>
              <!-- / header top left -->
              <div class="aa-header-top-right">
                <ul class="aa-head-top-nav-right">
                    
                  <!--/ <li class="hidden-xs"><a href="wishlist.html">Wishlist</a></li> -->

                  <li class="hidden-xs"><a href="cart.php">My Cart</a></li>
                  <li class="hidden-xs"><a href="checkout.php">Checkout</a></li>
                  
                  <?php if(isset($_SESSION["authenticationCodeCustomer"])){ ?>
                  <li><a href="my-account.php">My Account</a></li>
                  <?php } ?>

                  <?php if(!isset($_SESSION["authenticationCodeCustomer"])){ ?>
                  <li><a href="account.php" >Login or Register</a></li>
                  <?php }else{ ?>
                  <li><a href="logout.php" >Logout</a></li>
                  <?php } ?>
                  <!--<li><a href="" data-toggle="modal" data-target="#login-modal">Login</a></li>-->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-bottom-area">
              <!-- logo  -->
              <div class="aa-logo">
                <!-- Text based logo -->
                <a href="index.php">
                  <span class="fa fa-shopping-cart"></span>
                  <p>Boutique <strong>E-Store</strong> <span>Your Shopping Partner</span></p>
                </a>
                <!-- img based logo -->
                <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
              </div>
              <!-- / logo  -->
               <!-- cart box -->
              <div class="aa-cartbox">
                <a class="aa-cart-link" href="#">
                  <span class="fa fa-shopping-basket"></span>
                  <span class="aa-cart-title">SHOPPING CART</span>
                  <span class="aa-cart-notify"><?php echo (isset($_SESSION["cart_products"]))? count($_SESSION["cart_products"]): "";?></span>
                </a>
                <div class="aa-cartbox-summary">
                    <table width="100%" class="table table-responsive" cellpadding="6" cellspacing="0">
                     <thead>
                      <tr>
                        <th>Product</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    
                      <tbody>
                        <?php
                        if(isset($_SESSION["cart_products"])) //check session var
                        {
                            $currency = "Rs";
                            $shipping_cost = 0;
                            $total = 0; //set initial total value
                            $b = 0; //var for zebra stripe table 
                            $taxes = [0=>0];
                            foreach ($_SESSION["cart_products"] as $cart_itm)
                            {
                                //set variables to use in content below
                                $product_image = $cart_itm["product_image"];
                                $product_name = $cart_itm["product_name"];
                                $product_qty = $cart_itm["product_qty"];
                                $product_price = $cart_itm["product_price"];
                                $product_color = $cart_itm["product_color"];
                                $subtotal = ($product_price * $product_qty); //calculate Price x Qty

                                echo '<tr >';
                                echo '<td>'.$product_name.'<strong> x '.$product_qty.'</strong></td>';
                                echo '<td>'.$currency.$product_price.'</td>';
                                echo '</tr>';
                                $total = ($total + $subtotal); //add subtotal to total var
                            }

                            $grand_total = $total + $shipping_cost; //grand total including shipping cost
                            foreach($taxes as $key => $value){ //list and calculate all taxes in array
                                    $tax_amount     = round($total * ($value / 100));
                                    $tax_item[$key] = $tax_amount;
                                    $grand_total    = $grand_total + $tax_amount;  //add tax val to grand total
                            }

                            $list_tax       = '';
                            foreach($tax_item as $key => $value){ //List all taxes
                                $list_tax .= $key. ' : '. $currency. sprintf("%01.2f", $value).'<br />';
                            }
                        }
                        ?>
                      </tbody>
                      <tfoot>
                         <tr>
                          <th>Total</th>
                          <td>Rs.<?php echo $grand_total; ?></td>
                        </tr>
                      </tfoot>
                    </table>
<!--                  <ul>
                    <li>
                      <a class="aa-cartbox-img" href="#"><img src="img/woman-small-2.jpg" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Product Name</a></h4>
                        <p>1 x Rs.250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>
                    <li>
                      <a class="aa-cartbox-img" href="#"><img src="img/woman-small-1.jpg" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Product Name</a></h4>
                        <p>1 x Rs.250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>                    
                    <li>
                      <span class="aa-cartbox-total-title">
                        Total
                      </span>
                      <span class="aa-cartbox-total-price">
                        Rs.500
                      </span>
                    </li>
                  </ul>-->
                  <a class="aa-cartbox-checkout aa-primary-btn" href="checkout.php">Checkout</a>
                </div>
              </div>
              <!-- / cart box -->
              <!-- search box -->
              <div class="aa-search-box">
                <form action="product.php">
                  <input type="text" name="s" id="" placeholder="Search here ex. 'Khadi' ">
                  <button type="submit"><span class="fa fa-search"></span></button>
                </form>
              </div>
              <!-- / search box -->             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header bottom  -->
  </header>
  <!-- / header section -->
  <!-- menu -->
  <section id="menu">
    <div class="container">
      <div class="menu-area">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div>
          <div class="navbar-collapse collapse">
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <li><a href="index.php">Home</a></li>

                <li><a href="brands.php">Brands</a></li>
              
              </li>
              
              <li><a href="#">Category <span class="caret"></span></a>
                <ul class="dropdown-menu">  
                   <?php $cqry = mysqli_query($con, "SELECT * FROM categories"); ?>    
                    <?php while($cr = mysqli_fetch_object($cqry)){ ?>
                  <li><a href="product.php?category=<?php echo $cr->name; ?>"><?php echo $cr->name; ?></a></li> 
                    <?php } ?>
                   </ul>
                     
              <li><a href="contact.php">Contact</a></li>
              <!--<li><a href="track.php">Track Order</a></li>-->
            </ul>
           

          </div><!--/.nav-collapse -->
        </div>
      </div>       
    </div>
  </section>
  <!-- / menu -->
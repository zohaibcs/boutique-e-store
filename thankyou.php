<?php include("includes/header.php");  ?>
 

 <!-- Cart view section -->
 <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
           <div style="font-size: 50px; padding: 100px; text-align: center;">
               Thank You!
               <br/>
               <br/>
               <br/>
               <a href="index.php" class="btn btn-danger">Continue Shopping</a>
           </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->


  <?php include("includes/footer.php"); ?>
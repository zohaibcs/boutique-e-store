<?php
ob_start();
session_start(); //start session
include_once("admin/includes/configuration.php"); //include config file
//var_dump($_POST); die;
//add product to session or create new one

//back to return url
$return_url = (isset($_POST["return_url"]))?urldecode($_POST["return_url"]):''; //return url

if(isset($_POST["type"]) && $_POST["type"]=='add' && $_POST["product_qty"]>0){

    $id = $_POST["product_id"];
    foreach($_POST as $key => $value){ //add all post vars to new_product array
        $new_product[$key] = filter_var($value, FILTER_SANITIZE_STRING);
    }
    //remove unecessary vars
    unset($new_product['type']);
    unset($new_product['return_url']); 
    
    //we need to get product name and price from database.
    $statement = mysqli_query($con, "SELECT name, price FROM products WHERE id = '".$id."' LIMIT 1") or die(mysqli_error($con));
    
    
    while($r = mysqli_fetch_object($statement)){
        
        if(!empty($_POST["size"])){
            $checkQtyQr = mysqli_query($con, "SELECT ps.remaining_quantity FROM product_sizes as ps JOIN sizes as s ON s.id = ps.size_id AND s.short_name = '".$_POST["size"]."' WHERE ps.product_id = '".$id."'") or die(mysqli_error($con));
            $pss = mysqli_fetch_object($checkQtyQr);
            if($_POST["product_qty"] > $pss->remaining_quantity){
                $_SESSION["msg"] = "Selected quantity exceeds stock limit. Please decrease quantity.";
                header('LOCATION:'.$return_url);
                die;
            }
        }
   
        //fetch product name, price from db and add to new_product array
        $new_product["product_name"] = $r->name; 
        $new_product["product_price"] = $r->price;
        
        if(isset($_SESSION["cart_products"])){  //if session var already exist
            if(isset($_SESSION["cart_products"][$new_product['product_id']])) //check item exist in products array
            {
                unset($_SESSION["cart_products"][$new_product['product_id']]); //unset old array item
            }           
        }
        $_SESSION["cart_products"][$new_product['product_id']] = $new_product; //update or create product session with new item  
    } 
}


//update or remove items 
if(isset($_POST["product_qty"]) || isset($_POST["remove_code"]))
{
    //update item quantity in product session
    if(isset($_POST["product_qty"]) && is_array($_POST["product_qty"])){
        foreach($_POST["product_qty"] as $key => $value){
            if(is_numeric($value)){
                $_SESSION["cart_products"][$key]["product_qty"] = $value;
            }
        }
    }
    //remove an item from product session
    if(isset($_POST["remove_code"]) && is_array($_POST["remove_code"])){
        foreach($_POST["remove_code"] as $key){
            unset($_SESSION["cart_products"][$key]);
        }   
    }
}

header('Location:'.$return_url);
ob_end_flush();
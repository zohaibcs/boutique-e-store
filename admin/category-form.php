<?php include("includes/header.php"); 

$id = $_GET["id"];
if(!empty($id)){
    $sql = "SELECT * FROM `categories` WHERE id = '".$id."'";
    $qry = mysqli_query($sql);
    $r = mysqli_fetch_object($con, $qry);
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add/Update Category
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add/Update Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="Models/categories.php">
                <input type="hidden" name="id" value="<?php echo !empty($_GET["id"])?$_GET["id"]:"";?>"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="category-name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                      <input type="text" required="required" class="form-control" name="name" id="category-name" value="<?php echo $r->name; ?>" placeholder="Name">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                      <textarea name="description" class="form-control" placeholder="Description"><?php echo $r->description; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
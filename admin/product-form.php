<?php include("includes/header.php"); 

$id = $_GET["id"];
$psData = [];
if(!empty($id)){
    $sql = "SELECT * FROM `products` WHERE id = '".$id."'";
    $qry = mysqli_query($con, $sql);
    $r = mysqli_fetch_object($qry);
}

    $Ssql = "SELECT * FROM `sizes`";
    $Sqry = mysqli_query($con, $Ssql);

    $PSsql = "SELECT * FROM `product_sizes` WHERE product_id = '".$id."'";
    $PSqry = mysqli_query($con, $PSsql);
    
    while($PSr = mysqli_fetch_object($PSqry)){
        $psData[$PSr->size_id] = $PSr ;
    }
    
?>

<style>
    .qtyContainer input{
        width: 100%;
        margin-bottom: 2px;
    }
    
    .size-qty div{
        margin-bottom: 7px;
    }
    
    .color-container div{
        padding-left: 0px;
        margin-bottom: 5px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add/Update Product
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add/Update Product</h3>
            </div>
              <div class="notificationMsg">
            <?php echo !empty($_GET["msg"]) ? $_GET["msg"]:""; ?>
           </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="Models/add-update-product.php" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo !empty($_GET["id"])?$_GET["id"]:"";?>"/>
              <div class="box-body">
                       
                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-5">
                      <select name="active" class="form-control">
                          <option value="1" <?php echo ($r->active == 1)? "selected" : ""; ?>>Activate</option>
                          <option value="0" <?php echo ($r->active == 0)? "selected" : ""; ?>>Deactivate</option>
                      </select>
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" required="required" id="full_name" value="<?php echo $r->name; ?>" placeholder="Name">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="logo" class="col-sm-2 control-label">Product Image</label>
                  <div class="col-sm-10">
                      <?php 
                      if(!empty($id)){
                            $pqry = mysqli_query($con, "SELECT * FROM `product_images` WHERE product_id = '".$id."'") ;                          
                         
                     while($pr = mysqli_fetch_object($pqry)){
                         
                         if(isset($pr->image_name)){ ?>
                        <img src="<?php echo UPLOAD_PATH_DISPLAY_ADMIN.$pr->image_name ;?>" style="width: 100px;"/>
                        
                      <?php }}
                      echo "<br/><br/>";
                         }?>
                        
                    <input type="file" name="product_image[]" class="form-control" />
                    <input type="file" name="product_image[]" class="form-control" />
                    <input type="file" name="product_image[]" class="form-control" />
                    <input type="file" name="product_image[]" class="form-control" />
                    <input type="file" name="product_image[]" class="form-control" />
                    <input type="file" name="product_image[]" class="form-control" />
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="category_id" class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-4">
                      <?php $cqry = mysqli_query($con, "SELECT * FROM categories"); ?>
                      <select name="category_id" class="form-control" required="required">
                          <option value="0">Select Category</option>
                          <?php while($cr = mysqli_fetch_object($cqry)){ ?>
                          <option value="<?php echo $cr->id ?>" <?php echo ($cr->id == $r->category_id)? "selected" : ""; ?>><?php echo $cr->name ?></option>
                          <?php } ?>
                      </select>
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="price" class="col-sm-2 control-label">Price</label>
                  <div class="col-sm-4">
                    <input type="price" class="form-control" id="price" required="required" name="price" value="<?php echo $r->price; ?>" placeholder="Price">
                  PKR
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="size" class="col-sm-2 control-label">Size & Quantity</label>
                  <div class="col-sm-10">
                      <?php while($s = mysqli_fetch_object($Sqry)){ 
                          
                          $PSD = array_key_exists($s->id, $psData)? $psData[$s->id] : null ;
                          ?>
                            <div class="row">
                                <div class="col-sm-2 size-qty">
                                    <input type="checkbox" name="size[]" value="<?php echo $s->id; ?>" <?php echo !is_null($PSD) ? "checked" : ""; ?>/> <?php echo $s->short_name; ?> (<?php echo $s->name ; ?>)
                                </div>
                                <div class="col-sm-1 qtyContainer">
                                    <input type="text" name="quantity[]" value="<?php echo is_null($PSD)?"": $PSD->quantity; ?>" <?php echo is_null($PSD)? "style=\"visibility:hidden\"": ""; ?> id="<?php echo $s->id; ?>" />
                                </div>
                                <?php if(!is_null($PSD)){ ?>
                                <div class="col-sm-3">
                                    Remaining Quantity: <?php echo empty($PSD->remaining_quantity)? 0 : $PSD->remaining_quantity; ?>
                                </div>
                                <?php } ?>
                            </div>
                      <?php } ?>
                  </div>
                  </div>
                  <?php /* ?>
                  <div class="col-sm-2 size-qty">
                      <?php $sizes = explode(",", $r->size) ; ?>
                      <div class="">
                        <input type="checkbox" name="size[]" value="S" <?php echo (in_array("S", $sizes)? "checked" : ""); ?>/> S (Small) <br/>
                      </div>
                      <div class="">
                        <input type="checkbox" name="size[]" value="M" <?php echo (in_array("M", $sizes)? "checked" : ""); ?> /> M (Small)<br/>
                      </div>
                      <div class="">
                        <input type="checkbox" name="size[]" value="L" <?php echo (in_array("L", $sizes)? "checked" : ""); ?> /> L (Small)<br/>
                      </div>
                      <div class="">
                        <input type="checkbox" name="size[]" value="XL" <?php echo (in_array("XL", $sizes)? "checked" : ""); ?> /> XL (Single X L)<br/>
                      </div>
                      <div class="">
                        <input type="checkbox" name="size[]" value="XXL" <?php echo (in_array("XXL", $sizes)? "checked" : ""); ?> />XXL (Double X L)<br/>
                      </div>
                  </div>
                  <div class="col-sm-1 qtyContainer">
                      <?php $quantity = explode(",", $r->quantity) ; ?>
                      <input type="text" name="quantity[]" value="<?php echo array_key_exists(array_search("S", $sizes) ,$quantity)? $quantity[array_search("S", $sizes)]: ""; ?>" <?php echo !array_key_exists(array_search("S", $sizes) ,$quantity)? "style=\"visibility:hidden\"": ""; ?> id="S" />
                      <input type="text" name="quantity[]" value="<?php echo array_key_exists(array_search("M", $sizes) ,$quantity)? $quantity[array_search("M", $sizes)]: ""; ?>" <?php echo !array_key_exists(array_search("M", $sizes) ,$quantity)? "style=\"visibility:hidden\"": ""; ?> id="M" />
                      <input type="text" name="quantity[]" value="<?php echo array_key_exists(array_search("L", $sizes) ,$quantity)? $quantity[array_search("L", $sizes)]: ""; ?>" <?php echo !array_key_exists(array_search("L", $sizes) ,$quantity)? "style=\"visibility:hidden\"": ""; ?> id="L" />
                      <input type="text" name="quantity[]" value="<?php echo array_key_exists(array_search("XL", $sizes) ,$quantity)? $quantity[array_search("XL", $sizes)]: ""; ?>" <?php echo !array_key_exists(array_search("XL", $sizes) ,$quantity)? "style=\"visibility:hidden\"": ""; ?> id="XL" />
                      <input type="text" name="quantity[]" value="<?php echo array_key_exists(array_search("XXL", $sizes) ,$quantity)? $quantity[array_search("XXL", $sizes)]: ""; ?>" <?php echo !array_key_exists(array_search("XXL", $sizes) ,$quantity)? "style=\"visibility:hidden\"": ""; ?> id="XXL" />
                  </div>
                </div>
                  <?php */ ?>
                <div class="form-group">
                  <label for="sku_code" class="col-sm-2 control-label">SKU Code</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" required="required" name="sku_code" id="sku_code" value="<?php echo $r->sku_code; ?>" placeholder="SKU Code">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="color" class="col-sm-2 control-label">Color</label>
                  <div class="col-sm-10 color-container">
                      <?php if(!empty($id)){
                          $colors = explode(",", $r->color);
                          $no = 1;
                          foreach($colors as $c=> $v){
                          ?>
                        <div class="col-sm-2 colorFeildBox">
                              <input type="text" class="form-control" name="color[]" value="<?php echo $v; ?>" placeholder="Color"/>
                              <?php if($no > 1){ ?>
                              <a class="colorRemoveBtn"><i class="fa fa-remove"></i></a>
                              <?php } ?>
                        </div>
                        <?php $no++; } }else{ ?>
                      
                        <div class="col-sm-2 colorFeildBox">
                            <input type="text" class="form-control" required="required" name="color[]" value="" placeholder="Color"/>
                        </div>
                      <?php } ?>
                  </div>
                  <div class="col-sm-12">
                      <a href="javascript: void(0)" id="add_more" class="pull-right btn btn-success">Add More</a>
                      </div>
                </div>
                  
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                      <textarea class="form-control" name="description" ><?php echo $r->description; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
  <script type="text/template" id="colorTemplate">
      
    <div class="col-sm-2 colorFeildBox">
            <input type="text" class="form-control" name="color[]" value="" placeholder="Color" />
                        <a class="colorRemoveBtn"><i class="fa fa-remove"></i></a>
    </div>
 </script>
  <script>
$(document).ready(function(){
    
    $(".size-qty input[type=checkbox]").click(function(){
        if($(this).prop("checked")){
            $("#"+$(this).val()).val("").css({"visibility":"visible"});
        }else{
            $("#"+$(this).val()).css({"visibility":"hidden"});
        }
    });
    
    $("#add_more").click(function(){
       $(".color-container").append($("#colorTemplate").html()); 
    });
    
    $(document).on("click", ".colorRemoveBtn", function(){
       $(this).closest(".colorFeildBox").remove(); 
    });
    
});      
      </script>
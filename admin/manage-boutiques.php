<?php include("includes/header.php"); ?>
<?php 
    $sql = "SELECT * FROM `boutique`";
    $qry = mysqli_query($con, $sql);
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Boutiques
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Boutiques</h3>
              <a href="boutique-form.php" class="btn btn-success pull-right">New Boutique</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">Logo</th>
                  <th width="20%">Name</th>
                  <th width="20%">Email</th>
                  <th width="10%">City</th>
                  <th width="10%">Phone</th>
                  <th width="10%">Status</th>
                  <th width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                    <?php while($r = mysqli_fetch_object($qry)){?>
                        <tr>
                          <td><img src="<?php echo UPLOAD_PATH_DISPLAY_ADMIN.$r->logo ;?>" style="width: 100px;"/></td>
                          <td><?php echo $r->full_name; ?></td>
                          <td><?php echo $r->email; ?></td>
                          <td><?php echo $r->city; ?></td>
                          <td><?php echo $r->phone_no; ?></td>
                          <td><?php echo ($r->status == "Approved")? "<span class='statusBG bgGreen'>".$r->status."</span>" : "<span class='statusBG bgYellow'>".$r->status."</span>"; ?></td>
                          <td>
                              <a href="boutique-form.php?id=<?php echo $r->id ?>">Update</a>
                          </td>
                        </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
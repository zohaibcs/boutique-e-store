<?php include("includes/header.php"); 

$id = $_GET["id"];
if(!empty($id)){
    $sql = "SELECT * FROM `boutique` WHERE id = '".$id."'";
    $qry = mysqli_query($con, $sql);
    $r = mysqli_fetch_object($qry);
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add/Update Boutique
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add/Update Boutique</h3>
            </div>
              <div class="notificationMsg">
            <?php echo !empty($_GET["msg"]) ? $_GET["msg"]:""; ?>
           </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="Models/add-update-boutiques.php" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo !empty($_GET["id"])?$_GET["id"]:"";?>"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                      <select name="status" class="form-control">
                          <option value="Not Approved">Not Approved</option>
                          <option value="Approved" <?php echo ($r->status == "Approved")?"selected='selected'":""; ?>>Approved</option>
                          <option value="Deactive" <?php echo ($r->status == "Deactive")?"selected='selected'":""; ?>>Deactive</option>
                      </select>
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="full_name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="full_name" class="form-control" required="required" id="full_name" value="<?php echo $r->full_name; ?>" placeholder="Name">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="logo" class="col-sm-2 control-label">Logo</label>
                  <div class="col-sm-10">
                     <?php if(!empty($r->logo)){ ?>
                        <img src="<?php echo UPLOAD_PATH_DISPLAY_ADMIN.$r->logo ;?>" style="width: 100px;"/>
                        <br/><br/>
                     <?php }?>
                        
                    <input type="file" name="logo" class="form-control" id="logo">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" required="required" name="email" id="email" value="<?php echo $r->email; ?>" placeholder="EMail">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                      <input type="password" class="form-control" required="required" name="password" id="password" value="<?php echo $r->password; ?>" placeholder="Password">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone #</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="phone" required="required" name="phone" value="<?php echo $r->phone_no; ?>" placeholder="Phone #">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="city" class="col-sm-2 control-label">City</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="city" required="required" name="city" value="<?php echo $r->city; ?>" placeholder="City">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address</label>
                  <div class="col-sm-10">
                      <textarea class="form-control" id="address" name="address" placeholder="Address"><?php echo $r->address; ?></textarea>
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="introduction" class="col-sm-2 control-label">Introduction</label>
                  <div class="col-sm-10">
                      <textarea name="introduction" class="form-control" placeholder="Introduction"><?php echo $r->introduction; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
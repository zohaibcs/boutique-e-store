<?php include("includes/header.php"); ?>
<?php 
    $sql = "SELECT * FROM `categories`";
    $qry = mysqli_query($con, $sql);
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Categories
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Categories</h3>
              <a href="category-form.php" class="btn btn-success pull-right">New Category</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="80%">Name</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    <?php while($r = mysqli_fetch_object($qry)){?>
                        <tr>
                          <td><?php echo $r->name; ?></td>
                          <td>
                              <a href="category-form.php?id=<?php echo $r->id ?>">Update</a>
                          </td>
                        </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
<?php 
session_start();
include("../includes/configuration.php"); 
include("../helpers/functions.php"); 

$id = $_POST["id"];

if(isset($_POST["save"])){
    
    $name = SecureData($_POST["name"]);
    $sku_code = SecureData($_POST["sku_code"]);
    $price = SecureData($_POST["price"]);
    $size = array_values(array_filter($_POST["size"]));
    $color = implode(",", $_POST["color"]);
    $quantity = array_values(array_filter($_POST["quantity"]));
    $description = SecureData($_POST["description"]);
    $boutique_id = $_SESSION["id"];
    $category_id = $_POST["category_id"];
    $status = $_POST["active"];
    $isInsert = false;
        
if(empty($id)){
        $sql = "INSERT INTO `products` SET `name` = '".$name."'"
                . ", `price` = '".$price."'"
                . ", `sku_code` = '".$sku_code."'"
                . ", `color` = '".$color."'"
                . ", `category_id` = '".$category_id."'"
                . ", `boutique_id` = '".$boutique_id."'"
                . ", `description` = '".$description."'";
        
    }else{
        $sql = "UPDATE `products` SET  `name` = '".$name."', `active` = '".$status."'"
                . ", `price` = '".$price."'"
                . ", `sku_code` = '".$sku_code."'"
                . ", `color` = '".$color."'"
                . ", `category_id` = '".$category_id."'"
                . ", `boutique_id` = '".$boutique_id."'"
                . ", `description` = '".$description."'";
            $sql .= " WHERE id = '".$id."'";
            
    }
    
    $q = mysqli_query($con, $sql) or die(mysqli_error($con));
    if($q){
            
            if(empty($id)){
                $id = mysqli_insert_id($con);
            }

            if(count($_FILES['product_image']["name"]) && $_FILES['product_image']["name"][0] != ""){
                if(!empty($id)){
                    $qryImg = mysqli_query($con,"SELECT * FROM product_images WHERE product_id = '".$id."'");
                    if(mysqli_num_rows($qryImg)){
                        while($rImg = mysqli_fetch_object($qryImg)){
                            if(file_exists(UPLOAD_PATH.$rImg->image_name)){
                                unlink(UPLOAD_PATH.$rImg->image_name);
                            }
                        }
                        $qryD = mysqli_query($con, "DELETE FROM product_images WHERE product_id = '".$id."'");
                    }
                }
                
                for($i = 0; $i < count($_FILES['product_image']); $i++){
                    if($_FILES['product_image']['name'][$i] != ""){

                        $target_path = UPLOAD_PATH;
                        $image = time()."_".$_FILES['product_image']['name'][$i];

                        $target_path = $target_path . $image; 

                        if(move_uploaded_file($_FILES['product_image']['tmp_name'][$i], $target_path)) {
                            $sqlImg = "INSERT product_images SET image_name = '".$image."', product_id = '".$id."'";
                            $qryImG = mysqli_query($con, $sqlImg);
                        }
                    }
                }
            }
            
            if(!empty($id) && count($size)){
                mysqli_query($con, "DELETE FROM product_sizes WHERE product_id = '".$id."'");
                foreach($size as $k=>$v){
                    $psInsert = mysqli_query($con, "INSERT INTO product_sizes SET product_id = '".$id."', size_id = '".$v."', quantity = '".$quantity[$k]."'");
                }
            }
            
        redirect("../manage-products.php?msg=".SUCCESS_MSG);
    }else{
        redirect("../product-form.php?msg=".ERROR_MSG);
    }
}

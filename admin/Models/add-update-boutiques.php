<?php 

include("../includes/configuration.php"); 
include("../helpers/functions.php"); 

$id = $_POST["id"];

if(isset($_POST["save"])){
    
    $full_name = SecureData($_POST["full_name"]);
    $slug = GenerateSlug($_POST["full_name"]);
    $status = SecureData($_POST["status"]);
    $email = SecureData($_POST["email"]);
    $password = SecureData($_POST["password"]);
    $city = SecureData($_POST["city"]);
    $phone = SecureData($_POST["phone"]);
    $address = SecureData($_POST["address"]);
    $introduction = SecureData($_POST["introduction"]);

    $isInsert = false;
    
    if($_FILES['logo']['name'] != ""){

        $target_path = UPLOAD_PATH;
        $image = time()."_".$_FILES['logo']['name'];

        $target_path = $target_path . $image; 

        if(move_uploaded_file($_FILES['logo']['tmp_name'], $target_path)) {
                echo "The file ".  $image . " has been uploaded";
        } else{
                echo "There was an error uploading the file, please try again!";
        }
    }
        
if(empty($id)){
        $sql = "INSERT INTO `boutique` SET `full_name` = '".$full_name."', `slug` = '".$slug."'"
                . ", `status` = '".$status."'"
                . ", `city` = '".$city."'"
                . ", `email` = '".$email."'"
                . ", `password` = '".$password."'"
                . ", `phone_no` = '".$phone."'"
                . ", `address` = '".$address."'"
                . ", `introduction` = '".$introduction."'";
        
        if(!empty($image)){
            $sql .= ", `logo` = '".$image."'";
        }
        
        $isInsert = true;
        
    }else{
        $sql = "UPDATE `boutique` SET  `full_name` = '".$full_name."', `slug` = '".$slug."'"
                . ", `status` = '".$status."'"
                . ", `city` = '".$city."'"
                . ", `email` = '".$email."'"
                . ", `password` = '".$password."'"
                . ", `phone_no` = '".$phone."'"
                . ", `address` = '".$address."'"
                . ", `introduction` = '".$introduction."'";
        
            if(!empty($image)){
                $oldFileName = $_SESSION["logo"];
                if(file_exists(UPLOAD_PATH.$oldFileName)){
                    unlink(UPLOAD_PATH.$oldFileName);
                }
                $sql .= ", `logo` = '".$image."'";
            }
            
            $sql .= " WHERE id = '".$id."'";
            
            $sqlLogin = "UPDATE login_details SET '"
                    . " password = '".$password."' WHERE email = '".$email." AND member_id ='".$id."'";
            $qryLogin = mysqli_query($con, $sqlLogin);
    }
    
    $q = mysqli_query($con, $sql);
    if($q){
        if($isInsert){
            $id = mysqli_insert_id($con);
            $sqlLogin = "INSERT INTO login_details SET email = '".$email."'"
                    . ", member_id ='".$id."'"
                    . ", member_type ='boutique'"
                    . ", password = '".$password."'";
            $qryLogin = mysqli_query($con, $sqlLogin);
        }
        redirect("../manage-boutiques.php?msg=".SUCCESS_MSG);
    }else{
        redirect("../boutique-form.php?msg=".ERROR_MSG);
    }
}

<?php
ob_start();
session_start();
include("../includes/configuration.php");
include("../helpers/functions.php");

if(isset($_POST["login"])){
    
    $email = $_POST["email"];
    $password = $_POST["password"];

    if(!empty($email) && !empty($password)){
        $sql = "SELECT * FROM login_details WHERE email = '".$email."' AND password = '".$password."'";
        $qry = mysqli_query($con, $sql); 
        if(mysqli_num_rows($qry)){
                    $r = mysqli_fetch_object($qry);

            if($r->member_type == "boutique"){
                $sql2 = "SELECT * FROM boutique WHERE id = '".$r->member_id."'";
                $qry2 = mysqli_query($con, $sql2);
                $r2 = mysqli_fetch_object($qry2);
                if($r2->status == "Not Approved"){
                    redirect("../login.php?msg=Your account did not approved yet");
                }else if($r2->status == "Deactivate"){
                    redirect("../login.php?msg=Your account has been deactivated");
                }else{
                    $_SESSION["id"] = $r2->id;
                    $_SESSION["email"] = $r2->email;
                    $_SESSION["full_name"] = $r2->full_name;
                    $_SESSION["status"] = $r2->status;
                    $_SESSION["member_type"] = $r->member_type;
                    $_SESSION["authenticationCode"] = "@PassWoRd!@#456";

                    redirect("../dashboard-boutique.php");
                }
            }else if($r->member_type == "admin"){

                    $sql2 = "SELECT * FROM super_admin WHERE id = '".$r->member_id."'";
                    $qry2 = mysqli_query($con, $sql2);
                    $r2 = mysqli_fetch_object($qry2);
                    $_SESSION["id"] = $r2->id;
                    $_SESSION["email"] = $r2->email;
                    $_SESSION["full_name"] = $r2->full_name;
                    $_SESSION["status"] = "";
                    $_SESSION["member_type"] = $r->member_type;
                    $_SESSION["authenticationCode"] = "@PassWoRd!@#456";

                    redirect("../dashboard.php");
            }
        }else{
            redirect("../login.php?msg=Invalid username or password, Please try again.");
        }
    }

}
ob_end_flush();
?>
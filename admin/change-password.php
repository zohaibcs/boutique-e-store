<?php include("includes/header.php"); 

$id = $_GET["id"];
if(!empty($id)){
    $sql = "SELECT * FROM `categories` WHERE id = '".$id."'";
    $qry = mysqli_query($sql);
    $r = mysqli_fetch_object($con, $qry);
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Change Password</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="Models/change-password.php">
                <input type="hidden" name="id" value="<?php echo !empty($_GET["id"])?$_GET["id"]:"";?>"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="current-password" class="col-sm-2 control-label">Current Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="current-password" placeholder="Current Password">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="new-password" class="col-sm-2 control-label">New Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="new-password" placeholder="New Password">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="confirm-password" class="col-sm-2 control-label">Confirm Password</label>
                  <div class="col-sm-10">
                    <input type="confirm" class="form-control" id="confirm-password" placeholder="Confirm Password">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="change-password" class="btn btn-info pull-right">Change Password</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
<?php include("includes/header.php"); ?>
<?php 

    $newBqry = mysqli_query($con, "SELECT * FROM boutique WHERE status = 'Approved' ORDER BY id DESC LIMIT 10");
    $top10Pqry = mysqli_query($con, "SELECT COUNT('op.product_id') as pCount, p.name FROM ordered_products as op JOIN products as p ON p.id = op.product_id GROUP BY op.product_id LIMIT 10");
    $top10Bqry = mysqli_query($con, "SELECT COUNT('op.brand_id') as bCount, b.full_name FROM ordered_products as op JOIN boutique as b ON b.id = op.brand_id GROUP BY op.brand_id LIMIT 10");

    $dataSql = "SELECT SUM(p.price) as totalSales, COUNT(o.id) as ordersCount FROM `orders` as o "
        . "JOIN ordered_products as op ON op.order_id = o.id "
        . "JOIN `products` as p ON p.id = op.product_id WHERE o.status = 'Delivered' ";
    $dataQry = mysqli_query($con, $dataSql);
    $dataB = mysqli_fetch_object($dataQry);
    $totalSales = $dataB->totalSales;
    $ordersCount = $dataB->ordersCount;
    
    $totalBoutiques = mysqli_fetch_object(mysqli_query($con, "SELECT COUNT(id) as totalBoutiques FROM boutique WHERE status = 'Approved' ORDER BY id DESC LIMIT 10"))->totalBoutiques;
    
    $topsaleQry = "SELECT oop.sale, b.full_name FROM boutique as b LEFT JOIN (SELECT SUM(p.price) as sale, op.brand_id FROM orders as o JOIN ordered_products as op ON op.order_id = o.id JOIN products as p ON p.id = op.product_id WHERE o.status = 'Delivered' GROUP BY op.brand_id) as oop ON oop.brand_id = b.id ORDER BY oop.sale DESC";
    $topSalesB = mysqli_query($con, $topsaleQry);

    ?>

<style>
    .aboveInfo{
        font-size: 26px;
        margin-bottom: 10px;
    }
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="row aboveInfo">
            <div class="col-sm-4">
                <strong>Total Sales:</strong> PKR <?php echo empty($totalSales)? 0 : $totalSales; ?> 
            </div>
            <div class="col-sm-4">
                <strong>Total Boutiques:</strong> <?php echo $totalBoutiques; ?> 
            </div>
            <div class="col-sm-4">
                <strong>Total Orders Delivered:</strong> <?php echo $ordersCount; ?>
            </div>
        </div>
      
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                      <li class=""><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">New Boutiques</a></li>
                      <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Top 10 Boutiques</a></li>
                      <li class=""><a href="#tab_3-2" data-toggle="tab" aria-expanded="true">Top 10 products</a></li>
                      <li class="active"><a href="#tab_4-1" data-toggle="tab" aria-expanded="true">Brand's Sale</a></li>
                      <li class="pull-left header"><i class="fa fa-th"></i> Summary</li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane" id="tab_1-1">
                          <table class="table table-bordered table-striped">
                              <tr>
                                  <th style="width: 5%;">Sr#</th>
                                  <th>Name</th>
                                  <!--<th>Order Placed</th>-->
                              </tr>
                              <?php 
                                $no = 1;
                                while($nb = mysqli_fetch_object($newBqry)){ 
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $nb->full_name; ?></td>
                                  <!--<td><?php // echo $nb->pCount; ?></td>-->
                              </tr>
                              <?php $no++;} ?>
                          </table>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2-2">
                        <table class="table table-bordered table-striped">
                              <tr>
                                  <th style="width: 5%;">Sr#</th>
                                  <th>Name</th>
                                  <th style="width: 20%;">Count</th>
                              </tr>
                              <?php 
                                $no = 1;
                                while($tb = mysqli_fetch_object($top10Bqry)){ 
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $tb->full_name; ?></td>
                                  <td class="text-center"><?php echo $tb->bCount; ?></td>
                              </tr>
                              <?php $no++;} ?>
                          </table>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_3-2">
                        <table class="table table-bordered table-striped">
                              <tr>
                                  <th style="width: 5%;">Sr#</th>
                                  <th>Name</th>
                                  <th style="width: 20%;">Count</th>
                              </tr>
                              <?php 
                                $no = 1;
                                while($tp = mysqli_fetch_object($top10Pqry)){ 
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $tp->name; ?></td>
                                  <td class="text-center"><?php echo $tp->pCount; ?></td>
                              </tr>
                              <?php $no++;} ?>
                          </table>
                      </div>
                      <!-- /.tab-pane -->
                      <!-- /.tab-pane -->
                      <div class="tab-pane active" id="tab_4-1">
                          
                        <table class="table table-bordered table-striped">
                              <tr>
                                  <th style="width: 5%;">Sr#</th>
                                  <th>Name</th>
                                  <th style="width: 20%;">Total Sale (PKR)</th>
                              </tr>
                              <?php 
                                $no = 1;
                                while($tp = mysqli_fetch_object($topSalesB)){ 
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $tp->full_name; ?></td>
                                  <td class="text-center"><strong><?php echo empty($tp->sale)? 0 : $tp->sale ; ?></strong></td>
                              </tr>
                              <?php $no++;} ?>
                        </table>
                                                    
                      </div>
                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div>
            

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
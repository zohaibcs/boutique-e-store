<?php include("includes/header.php"); 

$id = $_GET["id"];
if(!empty($id)){
    $sql = "SELECT o.*, c.* FROM `orders` as o "
            . "JOIN customers as c ON c.id = o.customer_id "
            . "WHERE o.id = '".$id."' GROUP BY o.id";
//    echo $sql;
    $qry = mysqli_query($con, $sql);
    $r = mysqli_fetch_object($qry);
}
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Order Details
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="dashboard-boutique.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="manage-orders.php">Orders</a></li>
        <li class="active">Order Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Order Details</h3>
            </div>
              <div class="notificationMsg">
            <?php echo !empty($_GET["msg"]) ? $_GET["msg"]:""; ?>
           </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="Models/update-status.php" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo !empty($_GET["id"])?$_GET["id"]:"";?>"/>
              <div class="box-body">
                  
                <div class="form-group">
                  <label for="logo" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-3">
                      <?php 
                        $statuses = ["Pending", "Processing", "Cancelled", "Shipped", "Rejected", "Delivered"];
                        $display = false;
                      ?>
                      <select name="status" class="form-control">
                          <?php foreach($statuses as $k => $s){ 
                              if($r->status == $s){
                                  $display = true;
                              }
                              if($display){
                              ?>
                          <option value="<?php echo $s ; ?>" <?php echo ($r->status == $s)? "selected":""; ?>><?php echo $s; ?></option>
                          <?php } } ?>
                      </select>
                  </div>
                </div>
                            <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" class="btn btn-info pull-right">Save</button>
              </div>
                            
                            <fieldset>
                                <legend>Customer Detail</legend>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> Customer</label>
                                    <div class="col-sm-10">
                                        <?php echo $r->full_name; ?>
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> Email</label>
                                    <div class="col-sm-10">
                                        <?php echo $r->email; ?>
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> Phone No</label>
                                    <div class="col-sm-10">
                                        <?php echo $r->phone_no; ?>
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> Shipping Address</label>
                                    <div class="col-sm-10">
                                        <?php echo $r->shipping_address.", ".$r->shipping_city; ?>
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> Billing Address</label>
                                    <div class="col-sm-10">
                                        <?php echo $r->billing_address.", ".$r->billing_city; ?>
                                    </div>
                                  </div>

                            </fieldset>
                            
                            <?php
                            $Psql = "SELECT p.id as pid, p.name, p.price, op.* FROM ordered_products as op JOIN products as p ON p.id = op.product_id JOIN sizes as s ON s.short_name = op.size WHERE op.order_id = '".$id."'";
                                $sqlpqry = mysqli_query($con, $Psql) or die(mysqli_error($con));
                            while($pr = mysqli_fetch_object($sqlpqry)){
                                 $piqry = mysqli_query($con, "SELECT image_name FROM product_images WHERE product_id = '".$pr->pid."' LIMIT 1");
                                $pi = mysqli_fetch_object($piqry);
                                ?>
                            <fieldset>
                                <legend>Product</legend>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> Product Name</label>
                                    <div class="col-sm-10">
                                        <?php echo $pr->name; ?>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="logo" class="col-sm-2 control-label">Product Image</label>
                                    <div class="col-sm-10">
                                      <?php if(isset($pi->image_name)){ ?>
                                          <img src="<?php echo UPLOAD_PATH_DISPLAY_ADMIN.$pi->image_name ;?>" style="width: 100px;"/>
                                       <?php } ?>
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-1 control-label"> Size</label>
                                    <div class="col-sm-2" style="padding-top: 7px;">
                                        <?php echo $pr->size; ?>
                                    </div>
                                    <label for="name" class="col-sm-1 control-label"> Quantity</label>
                                    <div class="col-sm-1" style="padding-top: 7px;">
                                        <?php echo $pr->quantity; ?>
                                    </div>
                                    <label for="name" class="col-sm-1 control-label"> Color</label>
                                    <div class="col-sm-2" style="padding-top: 7px;">
                                        <?php echo $pr->color; ?>
                                    </div>
                                    <label for="name" class="col-sm-1 control-label"> Price</label>
                                    <div class="col-sm-1" style="padding-top: 7px;">
                                        <?php echo $pr->price; ?>
                                    </div>
                                    <label for="name" class="col-sm-1 control-label"> SubTotal</label>
                                    <div class="col-sm-1" style="padding-top: 7px;">
                                        <?php echo $pr->price * $pr->quantity; ?>
                                    </div>
                                  </div>
                            </fieldset>
                            <?php } ?>
                            
                            <fieldset>
                                <legend>Special Note</legend>
                                <div class="col-sm-12">
                                    <?php echo (empty($r->special_note))? "N/A" : $r->special_note; ?>
                                </div>
                            </fieldset>
                            <br>
                            <br>
                            <br>
                            <br>
                            
              <!-- /.box-footer -->
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
  <script  type="text/template" id="colorTemplate">
      
    <div class="col-sm-2">
            <input type="text" class="form-control" name="color[]" value="" placeholder="Color" />
    </div>
 </script>
  <script>
$(document).ready(function(){
    
    $(".size-qty input[type=checkbox]").click(function(){
        if($(this).prop("checked")){
            $("#"+$(this).val()).val("").css({"visibility":"visible"});
        }else{
            $("#"+$(this).val()).css({"visibility":"hidden"});
        }
    });
    
    $("#add_more").click(function(){
       $(".color-container").append($("#colorTemplate").html()); 
    });
});      
      </script>
<?php include("includes/header.php"); ?>
<?php 

        $sql = "SELECT COUNT(op.product_id) as pCount, o.*, c.full_name as customer_name, c.phone_no FROM `orders` as o "
            . "JOIN ordered_products as op ON op.order_id = o.id AND op.brand_id = '".$_SESSION["id"]."' "
            . "JOIN `customers` as c ON c.id = o.customer_id WHERE o.status = 'pending' GROUP BY o.id LIMIT 10";
    
    $ordersqry = mysqli_query($con, $sql) or die(mysqli_error($con));
    $top10Pqry = mysqli_query($con, "SELECT COUNT('op.product_id') as pCount, p.name FROM ordered_products as op JOIN products as p ON p.id = op.product_id WHERE op.brand_id = '".$_SESSION["id"]."' GROUP BY op.product_id LIMIT 10");

    $dataSql = "SELECT SUM(p.price) as totalSales FROM `orders` as o "
        . "JOIN ordered_products as op ON op.order_id = o.id AND op.brand_id = '".$_SESSION["id"]."' "
        . "JOIN `products` as p ON p.id = op.product_id WHERE o.status = 'Delivered' ";
    $dataQry = mysqli_query($con, $dataSql);
    $dataB = mysqli_fetch_object($dataQry);
    $totalSales = $dataB->totalSales;
    
    $productsLowStock = mysqli_query($con, "SELECT ps.remaining_quantity, s.short_name, p.name FROM products as p JOIN product_sizes as ps ON ps.product_id = p.id JOIN sizes as s ON s.id = ps.size_id WHERE ps.remaining_quantity <= 5 AND p.boutique_id = '".$_SESSION["id"]."'");
?>

<style>
    .aboveInfo{
        font-size: 26px;
        margin-bottom: 10px;
    }
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    
    <!-- Main content -->
    <section class="content">
        
        <div class="row aboveInfo">
            <div class="col-sm-4">
                <strong>Total Sales:</strong> PKR <?php echo empty($totalSales)? 0 : $totalSales; ?> 
            </div>
<!--            <div class="col-sm-4">
                Total Order Placed: <?php ?>
            </div>
            <div class="col-sm-4">
                Total Products: <?php ?>
            </div>-->
        </div>
      
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                      <li class=""><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">Low stock products</a></li>
                      <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Top 10 Products</a></li>
                      <li class="active"><a href="#tab_3-2" data-toggle="tab" aria-expanded="true">Orders</a></li>
                      <li class="pull-left header"><i class="fa fa-th"></i> Summary</li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane" id="tab_1-1">
                          <table class="table table-bordered table-striped">
                              <tr>
                                  <th style="width: 5%;">Sr#</th>
                                  <th>Name</th>
                                  <th style="width: 20%;">Size</th>
                                  <th style="width: 20%;">Count</th>
                              </tr>
                              <?php 
                                $no = 1;
                                while($tp = mysqli_fetch_object($productsLowStock)){ 
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $tp->name; ?></td>
                                  <td><?php echo $tp->short_name; ?></td>
                                  <td class="text-center" style="color: red; font-weight: bold;"><?php echo $tp->remaining_quantity; ?></td>
                              </tr>
                              <?php $no++;} ?>
                          </table>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2-2">
                       <table class="table table-bordered table-striped">
                              <tr>
                                  <th style="width: 5%;">Sr#</th>
                                  <th>Name</th>
                                  <th style="width: 20%;">Count</th>
                              </tr>
                              <?php 
                                $no = 1;
                                while($tp = mysqli_fetch_object($top10Pqry)){ 
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $tp->name; ?></td>
                                  <td class="text-center"><?php echo $tp->pCount; ?></td>
                              </tr>
                              <?php $no++;} ?>
                          </table>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane active" id="tab_3-2">
                        <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th width="10%">Products</th>
                          <th width="40%">Customer Name</th>
                          <th width="20%">Customer no</th>
                          <th width="10%">Status</th>
                          <th width="10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php while($r = mysqli_fetch_object($ordersqry)){?>
                                <tr>
                                    <td class="text-center"><?php echo $r->pCount; ?></td>
                                    <td><?php echo $r->customer_name; ?></td>
                                    <td><?php echo $r->phone_no; ?></td>
                                    <td><?php 
                                    if($r->status == "Pending"){
                                        echo "<span class='statusBG bgYellow'>".$r->status."</span>"; 
                                    }else if($r->status == "Canceled"){
                                        echo "<span class='statusBG bgRed'>".$r->status."</span>";
                                    }else if($r->status == "Processing"){
                                        echo "<span class='statusBG bgOrange'>".$r->status."</span>";
                                    }else{
                                        echo "<span class='statusBG bgGreen'>".$r->status."</span>";
                                    }
                                    ?>
                                    </td>
                                    <td>
                                        <a href="order-details.php?id=<?php echo $r->id; ?>">Details</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                      </table>
                      </div>
                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div>
            

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
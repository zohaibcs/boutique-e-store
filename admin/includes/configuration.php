<?php

error_reporting(0);

$host = "localhost";
$user = "root";
$password = "";
$database = "boutique_e_store";

$con = mysqli_connect($host, $user, $password, $database) ;

if(!$con){
    die("Mysql connection error");
}

define("BASE_PATH", "http://localhost/boutique-e-store/admin/");
define("UPLOAD_PATH", "../../uploads/");
define("UPLOAD_PATH_DISPLAY_ADMIN", "../uploads/");
define("UPLOAD_PATH_DISPLAY", "uploads/");

// Notification Messages
define("SUCCESS_MSG", "Information Saved successfully");
define("ERROR_MSG", "An error occurred");
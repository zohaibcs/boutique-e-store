<?php include("includes/header.php"); ?>
<?php 
    $sql = "SELECT p.*, c.name as category_name FROM `products` as p "
            . "JOIN `categories` as c ON c.id = p.category_id "
            . "WHERE p.boutique_id = '".$_SESSION["id"]."'";
    $qry = mysqli_query($con, $sql);
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Products
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if(!empty($_GET["msg"])){ ?>
              <div class="notification-container">
                  <?php echo $_GET["msg"];?>
              </div>
              <?php } ?>
        
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Products</h3>
              <a href="product-form.php" class="btn btn-success pull-right">New Product</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="20%">Product Image</th>
                  <th width="30%">Name</th>
                  <th width="10%">Category</th>
                  <th width="10%">Status</th>
                  <th width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                    <?php while($r = mysqli_fetch_object($qry)){ 
                        $piqry = mysqli_query($con, "SELECT image_name as image FROM product_images WHERE product_id = '".$r->id."' LIMIT 1");
                        $pi = mysqli_fetch_object($piqry);
                        ?>
                        <tr>
                          <td><img src="<?php echo (!empty($pi->image))? UPLOAD_PATH_DISPLAY_ADMIN.$pi->image : "" ;?>" style="width: 100px;"/></td>
                          <td><?php echo $r->name; ?></td>
                          <td><?php echo $r->category_name; ?></td>
                          <td><?php echo ($r->active == 1)? "<span class='statusBG bgGreen'>Active</span>" : "<span class='statusBG bgRed'>Deactivated</span>"; ?></td>
                          <td class="text-center">
                              <a href="product-form.php?id=<?php echo $r->id ?>" class="marginRight10"><i class="fa fa-pencil"></i></a>
                              <a href="Models/delete-record.php?id=<?php echo $r->id ?>&type=product" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  
  <script>
      $(document).ready(function(){
          $("#example1").DataTable();
      });
      </script>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
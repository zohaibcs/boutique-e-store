<?php include("includes/header.php"); ?>
<?php 
    $sql = "SELECT COUNT(op.product_id) as pCount, o.*, c.full_name as customer_name, c.phone_no FROM `orders` as o "
            . "JOIN ordered_products as op ON op.order_id = o.id AND op.brand_id = '".$_SESSION["id"]."' "
            . "JOIN `customers` as c ON c.id = o.customer_id GROUP BY o.id LIMIT 10";
    
    $qry = mysqli_query($con, $sql) or die(mysqli_error($con));
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Orders
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Orders</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="10%">Products</th>
                    <th width="40%">Customer Name</th>
                    <th width="10%">Customer no</th>
                    <th width="10%">Status</th>
                    <th width="10%">Date</th>
                    <th width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                    <?php while($r = mysqli_fetch_object($qry)){?>
                        <tr>
                            <td><?php echo $r->pCount; ?></td>
                            <td><?php echo $r->customer_name; ?></td>
                            <td><?php echo $r->phone_no; ?></td>
                            <td><?php 
                            if($r->status == "Pending"){
                                echo "<span class='statusBG bgYellow'>".$r->status."</span>"; 
                            }else if($r->status == "Canceled"){
                                echo "<span class='statusBG bgRed'>".$r->status."</span>";
                            }else if($r->status == "Processing"){
                                echo "<span class='statusBG bgOrange'>".$r->status."</span>";
                            }else{
                                echo "<span class='statusBG bgGreen'>".$r->status."</span>";
                            }
                            ?>
                            </td>
                            <td><?php echo date("d-m-Y", strtotime($r->created_at)); ?></td>
                            <td>
                                <a href="order-details.php?id=<?php echo $r->id; ?>">Details</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
    <script>
      $(document).ready(function(){
          $("#example1").DataTable();
      });
      </script>
  <!-- /.content-wrapper -->
  <?php include("includes/footer.php"); ?>
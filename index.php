<?php include("includes/header.php");  ?>
  <!-- Start slider -->
  <section id="aa-slider">
    <div class="aa-slider-area">
      <div id="sequence" class="seq">
         <ul class="seq-canvas">
            <!-- single slide item -->
            <li>
              <div class="seq-model">
                <img data-seq src="img\homepage\vivesalon11920x6001920x700.jpg"  />
              </div>
              <div class="seq-title">
               <span data-seq>Save Up to 5% Off</span>                
                <h2 data-seq>New Arrivals </h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
            <!-- single slide item -->
            <li>
              <div class="seq-model">
                <img data-seq src="img\homepage\saleshopmoreitemsaddedmin1920x700.jpg" alt="Wristwatch slide img" />
              </div>
              <div class="seq-title">
                <span data-seq>Save Up to 40% Off</span>                
                <h2 data-seq>Sale</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
           
            
            <!-- single slide item -->  
             <li>
              <div class="seq-model">
                <img data-seq src="img\homepage\1920x700.jpg" alt="Male Female slide img" />
              </div>
              <div class="seq-title">
                <span data-seq>Save Up to 50% Off</span>                
                <h2 data-seq>Best Collection</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>                   
          </ul>
        <!-- slider navigation btn -->
<!--
        <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
          <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
          <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
        </fieldset>
-->
      </div>
    </div>
  </section>
  <!-- / slider -->
  <!-- Start Promo section -->
  <section id="aa-promo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-promo-area">
            <div class="row">
              <!-- promo left -->
              <div class="col-md-5 no-padding">                
                <div class="aa-promo-left">
                  <div class="aa-promo-banner">                    
                    <img src="img\homepage\5450x450.jpg" alt="img">                    
                    <div class="aa-prom-content">
                      <span>75% Off</span>
                                       
                    </div>
                  </div>
                </div>
              </div>
              <!-- promo right -->
              <div class="col-md-7 no-padding">
                <div class="aa-promo-right">
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="img\homepage\4300x220.png" alt="img">                      
                      <div class="aa-prom-content">
                        <span>Exclusive Item</span>
                                             
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="img\homepage\images-1300x220.jpg" alt="img">                 
                      <div class="aa-prom-content">
                        <span>Sale Off</span>
                        <h4><a href="#">On Old Stock</a></h4>    
                                               
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">  
                   
                      <img src="img\homepage\images-2300x220.jpg " alt="img">                      
                      <div class="aa-prom-content">
                        <span>New Arrivals</span>
                       
                         
                                             
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">           
                      <img src="img\homepage\download300x220.jpg " alt="img">                        
                      <div class="aa-prom-content">
                        <span>25% Off</span>
                                           
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Promo section -->
  <!-- Products section -->
  <section id="aa-product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-product-area">
              <div class="aa-product-inner">
                <!-- start prduct navigation -->
                 <ul class="nav nav-tabs aa-products-tab">
                     <?php 
                     $no = 1;
                     $catqry = mysqli_query($con, "SELECT * FROM categories ORDER BY name LIMIT 5") or die(mysqli_error($con));
                     while($c = mysqli_fetch_object($catqry)){
                         $cats[] = ["id"=> $c->id, "name"=> $c->name];
                             ?>
                    <li <?php if($no == 1){ ?>class="active"<?php } ?>>
                        <a href="#<?php echo str_replace(" ", "", $c->name); ?>" data-toggle="tab"><?php echo $c->name ; ?></a>
                    </li>
                     <?php $no++; } ?>
                  
                 </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                      <?php 
                      $no = 0;
                      foreach($cats as $k=>$cat){ ?>
                    <!-- Start readytowear product category -->
                    <div class="tab-pane fade in <?php echo ($no == 0)? "active": ""; ?>" id="<?php echo str_replace(" ", "", $cat["name"]); ?>">
                      <ul class="aa-product-catg">
                        <!-- start single product item -->
                        <?php 
                            $pqry = mysqli_query($con, "SELECT p.*, b.full_name as bname, ps.remaining_quantity as stock FROM products as p "
                                                        . "JOIN boutique as b ON b.id = p.boutique_id "
                                                        . "LEFT JOIN product_sizes as ps ON ps.product_id = p.id "
                                                        . "WHERE p.category_id = '".$cat["id"]."' AND p.active = 1 AND b.status = 'Approved' LIMIT 9") or die(mysqli_error($con));
                            
                            while($p = mysqli_fetch_object($pqry)){
                                 $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$p->id."' LIMIT 1");
                        $pi = mysqli_fetch_object($piqry);
                        ?>
                        <li>
                            <form method="post" action="update-cart.php">
                                <input type="hidden" name="product_id" value="<?php echo $p->id; ?>"/>
                                <input type="hidden" name="product_image" value="<?php echo $pi->product_image; ?>"/>
                                <input type="hidden" name="brand_id" value="<?php echo $p->boutique_id; ?>"/>
                                <input type="hidden" name="type" value="add"/>
                                <input type="hidden" name="product_qty" value="1"/>
                                <input type="hidden" name="return_url" value="<?php echo $current_url; ?>">
                              <figure>
                                <a class="aa-product-img" href="product-detail.php?id=<?php echo $p->id; ?>">
                                    <img style="width: 100%;" src="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" alt="<?php echo $p->name; ?>">
                                </a>
                                  <?php if(array_search($p->id, array_column($_SESSION["cart_products"], 'product_id')) == false){ ?>
                                  <a class="aa-add-card-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                    <?php }else{ ?>
                                   <a class="aa-add-card-btn dont-add"><span class="fa fa-shopping-cart"></span>Already Added</a>
                                  <?php } ?>
                                  <figcaption>
                                  <h4 class="aa-product-title"><a href="product-detail.php?id=<?php echo $p->id; ?>"><?php echo $p->name; ?></a></h4>
                                  <span class="aa-product-price"><?php echo $p->bname; ?></span><br/>
                                  <span class="aa-product-price">In Stock: <?php echo (stock == "")? 0 : $p->stock; ?></span><br/>
                                  <span class="aa-product-price">Rs.<?php echo $p->price; ?></span>
                                </figcaption>
                              </figure>                        
<!--                          <div class="aa-product-hvr-content">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                            <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>-->
                          <!-- product badge -->
                          <!--<span class="aa-badge aa-sale" href="#">SALE!</span>-->
                          </form>
                        </li>
                        <?php } ?>                  
                      </ul>
                        <a class="aa-browse-btn"  href="brands.php">Browse all Brands <span class="fa fa-long-arrow-right"></span></a>
                    </div>
                        <?php $no++; } ?>   
                    <!-- / men product category -->
                  </div>
                  <!-- quick view modal -->                  
                  <div class="modal fade" id="quick-view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">                      
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <div class="row">
                            <!-- Modal view slider -->
                            <div class="col-md-6 col-sm-6 col-xs-12">                              
                              <div class="aa-product-view-slider">                                
                                <div class="simpleLens-gallery-container" id="demo-1">
                                  <div class="simpleLens-container">
                                      <div class="simpleLens-big-image-container">
                                          <a class="simpleLens-lens-image" data-lens-image="img/view-slider/large/polo-shirt-1.png">
                                              <img src="img/view-slider/medium/polo-shirt-1.png" class="simpleLens-big-image">
                                          </a>
                                      </div>
                                  </div>
                                  <div class="simpleLens-thumbnails-container">
                                      <a href="#" class="simpleLens-thumbnail-wrapper"
                                         data-lens-image="img/view-slider/large/polo-shirt-1.png"
                                         data-big-image="img/view-slider/medium/polo-shirt-1.png">
                                          <img src="img/view-slider/thumbnail/polo-shirt-1.png">
                                      </a>                                    
                                      <a href="#" class="simpleLens-thumbnail-wrapper"
                                         data-lens-image="img/view-slider/large/polo-shirt-3.png"
                                         data-big-image="img/view-slider/medium/polo-shirt-3.png">
                                          <img src="img/view-slider/thumbnail/polo-shirt-3.png">
                                      </a>

                                      <a href="#" class="simpleLens-thumbnail-wrapper"
                                         data-lens-image="img/view-slider/large/polo-shirt-4.png"
                                         data-big-image="img/view-slider/medium/polo-shirt-4.png">
                                          <img src="img/view-slider/thumbnail/polo-shirt-4.png">
                                      </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- Modal view content -->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="aa-product-view-content">
                                <h3>T-Shirt</h3>
                                <div class="aa-price-block">
                                  <span class="aa-product-view-price">$34.99</span>
                                  <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!</p>
                                <h4>Size</h4>
                                <div class="aa-prod-view-size">
                                  <a href="#">S</a>
                                  <a href="#">M</a>
                                  <a href="#">L</a>
                                  <a href="#">XL</a>
                                </div>
                                <div class="aa-prod-quantity">
                                  <form action="">
                                    <select name="" id="">
                                      <option value="0" selected="1">1</option>
                                      <option value="1">2</option>
                                      <option value="2">3</option>
                                      <option value="3">4</option>
                                      <option value="4">5</option>
                                      <option value="5">6</option>
                                    </select>
                                  </form>
                                  <p class="aa-prod-category">
                                    Category: <a href="#">Polo T-Shirt</a>
                                  </p>
                                </div>
                                <div class="aa-prod-view-bottom">
                                  <a href="#" class="aa-add-to-cart-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                  <a href="#" class="aa-add-to-cart-btn">View Details</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>                        
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- / quick view modal -->              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Products section -->
  <!-- banner section -->
  <section id="aa-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">        
          <div class="row">
            <div class="aa-banner-area">
            <a href="#"><img src="img/fashion-banner.jpg" alt="fashion banner img"></a>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- popular section -->
  <section id="aa-popular-category">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-popular-category-area">
              <!-- start prduct navigation -->
             <ul class="nav nav-tabs aa-products-tab">
                <li class="active"><a href="#popular" data-toggle="tab">Popular</a></li>
                <li><a href="#featured" data-toggle="tab">Featured</a></li>
                <li><a href="#latest" data-toggle="tab">Latest</a></li>                    
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <!-- Start men popular category -->
                <div class="tab-pane fade in active" id="popular">
                  <ul class="aa-product-catg aa-popular-slider">
                    <!-- start single product item -->
                    <?php 
                            $pqry = mysqli_query($con, "SELECT p.* FROM products as p "
                                                        . "JOIN boutique as b ON b.id = p.boutique_id "
                                                        . "WHERE p.active = 1 AND b.status = 'Approved' "
                                                        . "ORDER BY rand() LIMIT 4");
                            
                            while($p = mysqli_fetch_object($pqry)){
                                
                                 $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$p->id."' LIMIT 1");
                        $pi = mysqli_fetch_object($piqry);
                        ?>
                        <li>
                            <form method="post" action="update-cart.php">
                            <input type="hidden" name="product_id" value="<?php echo $p->id; ?>"/>
                            <input type="hidden" name="product_image" value="<?php echo $pi->product_image; ?>"/>
                            <input type="hidden" name="brand_id" value="<?php echo $p->boutique_id; ?>"/>
                            <input type="hidden" name="type" value="add"/>
                            <input type="hidden" name="product_qty" value="1"/>
                            <input type="hidden" name="return_url" value="<?php echo $current_url; ?>">
                          <figure>
                            <a class="aa-product-img" href="product-detail.php?id=<?php echo $p->id; ?>">
                                <img src="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" alt="<?php echo $p->name; ?>">
                            </a>
                              <?php if(array_search($p->id, array_column($_SESSION["cart_products"], 'product_id')) == false){ ?>
                                  <a class="aa-add-card-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                    <?php }else{ ?>
                                   <a class="aa-add-card-btn dont-add"><span class="fa fa-shopping-cart"></span>Already Added</a>
                                  <?php } ?>
                              <figcaption>
                                <h4 class="aa-product-title"><a href="product-detail.php?id=<?php echo $p->id; ?>"><?php echo $p->name; ?></a></h4>
                                <span class="aa-product-price">Rs.<?php echo $p->price; ?></span>
                              </figcaption>
                          </figure>                        
<!--                          <div class="aa-product-hvr-content">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                            <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>-->
                          <!-- product badge -->
                          <!--<span class="aa-badge aa-sale" href="#">SALE!</span>-->
                          </form>
                        </li>
                        <?php } ?>                                                                                      
                  </ul>
                  <a class="aa-browse-btn" href="product.php">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                </div>
                <!-- / popular product category -->
                
                <!-- start featured product category -->
                <div class="tab-pane fade" id="featured">
                 <ul class="aa-product-catg aa-featured-slider">
                     <?php 
                            $pqry = mysqli_query($con, "SELECT p.* FROM products as p "
                                                        . "JOIN boutique as b ON b.id = p.boutique_id "
                                                        . "WHERE p.active = 1 AND b.status = 'Approved'  "
                                                        . "ORDER BY rand() LIMIT 4");
                            
                            while($p = mysqli_fetch_object($pqry)){
                                
                                $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$p->id."' LIMIT 1");
                                $pi = mysqli_fetch_object($piqry);
                        ?>
                        <li>
                            <form method="post" action="update-cart.php">
                            <input type="hidden" name="product_id" value="<?php echo $p->id; ?>"/>
                            <input type="hidden" name="product_image" value="<?php echo $pi->product_image; ?>"/>
                            <input type="hidden" name="brand_id" value="<?php echo $p->boutique_id; ?>"/>
                            <input type="hidden" name="type" value="add"/>
                            <input type="hidden" name="product_qty" value="1"/>
                            <input type="hidden" name="return_url" value="<?php echo $current_url; ?>">
                          <figure>
                            <a class="aa-product-img" href="product-detail.php?id=<?php echo $p->id; ?>">
                                <img src="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" alt="<?php echo $p->name; ?>">
                            </a>
                              <?php if(array_search($p->id, array_column($_SESSION["cart_products"], 'product_id')) == false){ ?>
                                  <a class="aa-add-card-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                    <?php }else{ ?>
                                   <a class="aa-add-card-btn dont-add"><span class="fa fa-shopping-cart"></span>Already Added</a>
                                  <?php } ?>
                              <figcaption>
                              <h4 class="aa-product-title"><a href="product-detail.php?id=<?php echo $p->id; ?>"><?php echo $p->name; ?></a></h4>
                              <span class="aa-product-price">Rs.<?php echo $p->price; ?></span>
                            </figcaption>
                          </figure>                        
<!--                          <div class="aa-product-hvr-content">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                            <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>-->
                          <!-- product badge -->
                          <!--<span class="aa-badge aa-sale" href="#">SALE!</span>-->
                          </form>
                        </li>
                        <?php } ?>                                                                                    
                  </ul>
                  <a class="aa-browse-btn" href="product.php">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                </div>
                <!-- / featured product category -->

                <!-- start latest product category -->
                <div class="tab-pane fade" id="latest">
                  <ul class="aa-product-catg aa-latest-slider">
                    <?php 
                            $pqry = mysqli_query($con, "SELECT p.* FROM products as p "
                                                        . "JOIN boutique as b ON b.id = p.boutique_id "
                                                        . " WHERE p.active = 1 AND b.status = 'Approved'  "
                                                        . "ORDER BY rand() LIMIT 4");
                            
                            while($p = mysqli_fetch_object($pqry)){
                                
                                $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$p->id."' LIMIT 1");
                                $pi = mysqli_fetch_object($piqry);
                        ?>
                        <li>
                            <form method="post" action="update-cart.php">
                            <input type="hidden" name="product_id" value="<?php echo $p->id; ?>"/>
                            <input type="hidden" name="product_image" value="<?php echo $pi->product_image; ?>"/>
                            <input type="hidden" name="brand_id" value="<?php echo $p->boutique_id; ?>"/>
                            <input type="hidden" name="type" value="add"/>
                            <input type="hidden" name="product_qty" value="1"/>
                            <input type="hidden" name="return_url" value="<?php echo $current_url; ?>">
                          <figure>
                            <a class="aa-product-img" href="product-detail.php?id=<?php echo $p->id; ?>">
                                <img src="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" alt="<?php echo $p->name; ?>">
                            </a>
                              <?php if(array_search($p->id, array_column($_SESSION["cart_products"], 'product_id')) == false){ ?>
                                  <a class="aa-add-card-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                    <?php }else{ ?>
                                   <a class="aa-add-card-btn dont-add"><span class="fa fa-shopping-cart"></span>Already Added</a>
                                  <?php } ?>
                              <figcaption>
                                <h4 class="aa-product-title"><a href="product-detail.php?id=<?php echo $p->id; ?>"><?php echo $p->name; ?></a></h4>
                                <span class="aa-product-price">Rs.<?php echo $p->price; ?></span>
                              </figcaption>
                          </figure>                        
<!--                          <div class="aa-product-hvr-content">
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
                            <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>-->
                          <!-- product badge -->
                          <!--<span class="aa-badge aa-sale" href="#">SALE!</span>-->
                          </form>
                        </li>
                        <?php } ?>                                                                                     
                  </ul>
                   <a class="aa-browse-btn" href="product.php">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                </div>
                <!-- / latest product category -->              
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </section>
  <!-- / popular section -->
  <!-- Support section -->
  <section id="aa-support">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-support-area">
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-truck"></span>
                <h4>FREE SHIPPING</h4>
                <P>Orders are packed and shipped Monday-Friday only. Most orders are shipped within 24 hours from the order date. Orders placed on the weekend and select holidays are processed on the next business day.
</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-clock-o"></span>
                <h4>30 DAYS MONEY BACK</h4>
                <P>Online purchases made through boutique-estore.com are valid for exchange, credit, or refund within 30 days from the ship date. Your return form and invoice provide a postmark date that specifies the last day you have to return your merchandise by mail. Refunds are only available for online purchases returned by mail. 
</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-phone"></span>
                <h4>SUPPORT 24/7</h4>
                <P>Our Online Customer Agent is always there to assist you.</P>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Support section -->
   

  <?php include("includes/footer.php"); ?>
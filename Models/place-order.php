<?php
session_start();
include("../admin/includes/configuration.php");
include("../admin/helpers/functions.php");

if(!empty($_POST["shipping_address"])){
    
    $email = (isset($_SESSION["cemail"])) ? $_SESSION["cemail"] : $_POST["email"];
    $password = $_POST["password"];
    $full_name = $_POST["full_name"];
    $phone_no = $_POST["phone_no"];
    $shipping_address = $_POST["shipping_address"];
    $shipping_city = $_POST["shipping_city"];    
    $billing_address = $_POST["billing_address"];
    $billing_city = $_POST["billing_city"];
    $special_note = $_POST["special_note"];
    
    if(!empty($email)){
       
        $cq = mysqli_query($con, "SELECT id FROM customers WHERE email = '".$email."'") or die(mysqli_error($con));
        if(mysqli_num_rows($cq)){
            $cr = mysqli_fetch_object($cq);
             $csql = "UPDATE customers SET full_name = '".$full_name."', "
                    . "phone_no='".$phone_no."', "
                    . "shipping_city='".$shipping_city."', "
                    . "shipping_address='".$shipping_address."', "
                    . "billing_city='".$billing_city."', "
                    . "billing_address='".$billing_address."' WHERE `id` = '".$cr->id."'" ;
             $customer_id = $cr->id;
        }else{
            $csql = "INSERT INTO customers SET full_name = '".$full_name."', "
                    . "email='".$email."', "
                    . "password='".$password."', "
                    . "phone_no='".$phone_no."', "
                    . "shipping_city='".$shipping_city."', "
                    . "shipping_address='".$shipping_address."', "
                    . "billing_city='".$billing_city."', "
                    . "billing_address='".$billing_address."'" ;
            
            $isInsert = true;
        }
        
        $cqsave = mysqli_query($con, $csql) or die(mysqli_error($con));
        
        if($cqsave){
            $customerId = (!empty($customer_id))? $customer_id : mysqli_insert_id($con);
            if($isInsert){
                $ldq = mysqli_query($con, "INSERT INTO login_details SET member_id = '".$customerId."', member_type='customer', email = '".$email."', password = '".$password."'") or die(mysqli_error($con));                
            }
            
            $oqry = mysqli_query($con,"INSERT INTO orders SET customer_id = '".$customerId."', status = 'Pending', `special_note` = '".$special_note."', created_at = '".date("Y-m-d")."'") or die("Order: ".mysqli_error($con));
            
            if($oqry){
                $orderId = mysqli_insert_id($con);
                $products = $_SESSION["cart_products"];
                $priceTotal = 0;
                foreach($products as $p){
                    $product_id = $p["product_id"];
                    $color = $p["color"];
                    $size = $p["size"];
                    $brand_id = $p["brand_id"];
                    $product_qty = $p["product_qty"];
                    $pTotal = $product_price * $product_qty;
                    $priceTotal = $pTotal + $priceTotal;
                    
                    $opqry = mysqli_query($con, "INSERT INTO ordered_products SET order_id = '".$orderId."', "
                            . "product_id = '".$product_id."', brand_id = '".$brand_id."', color = '".$color."', size = '".$size."', quantity = '".$product_qty."'") or die(mysqli_error($con));
                    
                    
                    $size_id = mysqli_fetch_object(mysqli_query($con, "SELECT id FROM sizes WHERE short_name = '".$size."'"))->id;
                    $pData = mysqli_fetch_object(mysqli_query($con, "SELECT remaining_quantity FROM product_sizes WHERE product_id = '$product_id' AND size_id = '".$size_id."'"));
                    $remaning_stock = $pData->remaining_quantity - $product_qty;
                    $pUData = mysqli_query($con, "UPDATE product_sizes SET remaining_quantity = '$remaning_stock' WHERE product_id = '$product_id' AND size_id = '".$size_id."'");
                    
                }
                
                $odpqry = mysqli_query($con, "UPDATE orders SET total = '".$priceTotal."' WHERE id = '".$orderId."'") or die(mysqli_error($con));
                
                if($odpqry){
                    $_SESSION["cart_products"] = [];
                    redirect("../thankyou.php");
                }
            }
        }
    }

}

?>
<?php include("helpers/functions.php");  ?>
<?php include("includes/header.php");  ?>

<?php 

    $item_per_page = 9;
    $page_url = "product.php";
    
    if(isset($_GET["page"])){ //Get page number from $_GET["page"]
        $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
    }else{
        $page_number = 1; //if there's no page number, set it to 1
    }

    
    $productsSql = "SELECT p.*, c.name as category_name, b.full_name FROM products as p "
                 . "LEFT JOIN boutique as b ON b.id = p.boutique_id "
                 . "LEFT JOIN categories as c ON c.id = p.category_id ";
    if(!empty($_GET["brand"])){
        $whereConditions[] = "b.full_name LIKE '%".$_GET["brand"]."%'";
    }
    
    if(!empty($_GET["category"])){
        $whereConditions[] = "c.name LIKE '%".$_GET["category"]."%'";
    }
    
    if(!empty($_GET["price_start"])){
        $whereConditions[] = "p.price >= '".$_GET["price_start"]."'";
    }
    
    if(!empty($_GET["price_end"])){
        $whereConditions[] = "p.price <= '".$_GET["price_end"]."'";
    }
    
    if(!empty($_GET["s"])){
        $whereConditions[] = "(p.name LIKE '%".$_GET["s"]."%' OR b.full_name LIKE '%".$_GET["s"]."%')";
    }
        
    if(!empty($_GET["sort_by"])){
        if($_GET["sort_by"] == "price"){
            $orderBy = " ORDER BY p.price ASC";
        }else{
            $orderBy = " ORDER BY p.name ASC";
        }
    }
    
    if(!empty($whereConditions)){
        $conditions = implode(" AND ", $whereConditions);
        $conditions .= " AND p.active = 1 AND b.status = 'Approved'" ;
    }else{
        $conditions = "p.active = 1 AND b.status = 'Approved'";
    }
    
    $productsSql .= " WHERE ".$conditions;
    
    $pqryForNums = mysqli_query($con, $productsSql);
    
    $page_position = (($page_number-1) * $item_per_page); 
    $productsSql .= $orderBy." LIMIT $page_position, $item_per_page";
    $pqry = mysqli_query($con, $productsSql) or die(mysqli_error($con));
    
    $get_total_rows = mysqli_num_rows($pqryForNums);
   
    $total_pages = ceil($get_total_rows/$item_per_page);
    
    $return_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

?>


  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Fashion</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
         <li><a href="brands.php">Brands</a></li>         
          <li class="active">Products</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            <div class="aa-product-catg-head">
              <div class="aa-product-catg-head-left">
                <form action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="GET" class="aa-sort-form">
                  <label for="">Sort by</label>
                  <select name="sort_by" onchange="SortBy(this)">
                    <option value="" selected="Default">Default</option>
                    <option value="name" <?php echo ($_GET["sort_by"] == 'name')?"selected":""; ?>>Name</option>
                    <option value="price" <?php echo ($_GET["sort_by"] == 'price')?"selected":""; ?>>Price</option>
                  </select>
                </form>
<!--                <form action="" class="aa-show-form">
                  <label for="">Show</label>
                  <select name="">
                    <option value="1" selected="12">12</option>
                    <option value="2">24</option>
                    <option value="3">36</option>
                  </select>
                </form>-->
              </div>
                <?php if(strpos($_SERVER['REQUEST_URI'], "?")){
                    if(!empty($_GET["brand"])){
                        $goToUrl = "product.php?brand=".$_GET["brand"];
                    }else{
                        $goToUrl = "product.php";
                    }
                    ?>
                <a href="<?php echo $goToUrl ; ?>" class="btn btn-danger" style=" margin-left: 20px;">Clear Filters</a>
                <?php } ?>
              
              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>
            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
                  <?php while($pr = mysqli_fetch_object($pqry)){ 
                       $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$pr->id."' LIMIT 1");
                        $pi = mysqli_fetch_object($piqry);
                      ?>
                <!-- start single product item -->
                <li>
                    <form method="post" action="update-cart.php">
                        <input type="hidden" name="product_id" value="<?php echo $pr->id; ?>"/>
                        <input type="hidden" name="product_image" value="<?php echo $pr->product_image; ?>"/>
                        <input type="hidden" name="brand_id" value="<?php echo $pr->boutique_id; ?>"/>
                        <input type="hidden" name="type" value="add"/>
                        <input type="hidden" name="product_qty" value="1"/>
                        <input type="hidden" name="return_url" value="<?php echo $current_url; ?>">
                        <figure>
                          <a class="aa-product-img" href="product-detail.php?id=<?php echo $pr->id; ?>">
                              <img src="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image ; ?>" style="width: 100%; height: 260px;" alt="">
                          </a>
                            <?php if(array_search($pr->id, array_column($_SESSION["cart_products"], 'product_id')) == false){ ?>
                                  <a class="aa-add-card-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                    <?php }else{ ?>
                                   <a class="aa-add-card-btn dont-add"><span class="fa fa-shopping-cart"></span>Already Added</a>
                                  <?php } ?>
                          <figcaption>
                              <h4 class="aa-product-title"><a href="product-detail.php?id=<?php echo $pr->id; ?>"><?php echo $pr->name ; ?></a></h4>
                            <span class="aa-product-price">Rs.<?php echo $pr->price ; ?></span>
                            <p class="aa-product-descrip"><?php echo $pr->description ; ?></p>
                          </figcaption>
                        </figure>                         
                
                  <!-- product badge -->
                  <!--<span class="aa-badge aa-sale" href="#">SALE!</span>-->
                  </form>
                </li>
                  <?php } ?>
              </ul>
              
            </div>
            <div class="aa-product-catg-pagination">
              <nav>
                  <?php echo paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url); ?>
<!--                
                  <ul class="pagination">
                  <li>
                    <a href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li>
                    <a href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>-->
              </nav>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
          <aside class="aa-sidebar">
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Category</h3>
              <ul class="aa-catg-nav">
                <?php $cqry = mysqli_query($con, "SELECT * FROM categories"); ?>    
                <?php while($cr = mysqli_fetch_object($cqry)){ 
                    if(strpos($_SERVER['REQUEST_URI'], "?")){
                        $filterUrl = $_SERVER['REQUEST_URI']."&category=";
                    }else{
                        $filterUrl = $_SERVER['REQUEST_URI']."?category=";
                    }
                    ?>
                <li><a href="<?php echo $filterUrl.$cr->name; ?>"><?php echo $cr->name; ?></a></li> 
                <?php } ?>
              </ul>
            </div>
          
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Shop By Price</h3>              
              <!-- price range -->
              <div class="aa-sidebar-price-range">
                  <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="get">
<!--                  <div id="skipstep" class="noUi-target noUi-ltr noUi-horizontal noUi-background">
                  </div>-->
<!--                  <span id="skip-value-lower" class="example-val">300.00</span>
                 <span id="skip-value-upper" class="example-val">10000.00</span>-->
                <input type="text" name="price_start" value="<?php echo $_GET["price_start"]; ?>" style="width: 50px"/>
                <input type="text" name="price_end" value="<?php echo $_GET["price_end"]; ?>" style="width: 50px"/>
                 <button class="aa-filter-btn" type="submit">Filter</button>
               </form>
              </div>              

            </div>
           
            <div class="aa-sidebar-widget">
              <h3>Recently Views</h3>
              <div class="aa-recently-views">
                <ul>
                    <?php 
                        $pqry = mysqli_query($con, "SELECT p.*, pi.image_name as product_image FROM products as p "
                                                    . "LEFT JOIN product_images as pi ON p.id = pi.product_id "
                                                    . "ORDER BY rand() LIMIT 4");

                        while($p = mysqli_fetch_object($pqry)){
                    ?>
                  <li>
                      <a href="product-detail.php?id=<?php echo $p->id; ?>" class="aa-cartbox-img">
                          <img alt="" src="<?php echo UPLOAD_PATH_DISPLAY.$p->product_image; ?>">
                      </a>
                    <div class="aa-cartbox-info">
                      <h4><a href="product-detail.php?id=<?php echo $p->id; ?>"><?php echo $p->name; ?></a></h4>
                      <p>Rs.<?php echo $p->price; ?></p>
                    </div>                    
                  </li>  
                            <?php } ?>
                </ul>
              </div>                            
            </div>
            <!-- single sidebar -->
<!--            <div class="aa-sidebar-widget">
              <h3>Top Rated Products</h3>
              <div class="aa-recently-views">
                <ul>
                  <li>
                    <a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-2.jpg"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="#">Product Name</a></h4>
                      <p>1 x Rs.2500</p>
                    </div>                    
                  </li>
                  <li>
                    <a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-1.jpg"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="#">Product Name</a></h4>
                      <p>1 x Rs.2500</p>
                    </div>                    
                  </li>
                   <li>
                    <a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-2.jpg"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="#">Product Name</a></h4>
                      <p>1 x Rs.2500</p>
                    </div>                    
                  </li>                                      
                </ul>
              </div>                            
            </div>-->
          </aside>
        </div>
       
      </div>
    </div>
  </section>
  <!-- / product category -->


  <?php include("includes/footer.php"); ?>
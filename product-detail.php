<?php include("includes/header.php");  ?>
<?php 
    
    $id = $_GET["id"];
    $productSql = "SELECT p.*, c.name as category_name, b.full_name, ps.remaining_quantity as stock FROM products as p "
                 . "LEFT JOIN boutique as b ON b.id = p.boutique_id "
                 . "LEFT JOIN categories as c ON c.id = p.category_id "
                 . "LEFT JOIN product_sizes as ps ON ps.product_id = p.id "
            . "WHERE p.id = '".$id."' AND p.active = 1";
    
    $pqry = mysqli_query($con, $productSql);
    $r = mysqli_fetch_object($pqry);
    
    $return_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
?>
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2><?php echo $r->name; ?></h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>     
          <li><a href="brands.php">Brands</a></li>         
          <li><a href="product.php?brand=<?php echo $r->full_name; ?>"><?php echo $r->full_name; ?></a></li>
          <li class="active"><?php echo $r->name; ?></li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-details">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-product-details-area">
            <div class="aa-product-details-content">
              <div class="row">
                <!-- Modal view slider -->
                <div class="col-md-5 col-sm-5 col-xs-12">                              
                  <div class="aa-product-view-slider">                                
                    <div id="demo-1" class="simpleLens-gallery-container">
                      <div class="simpleLens-container">
                          <?php 
                           $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$r->id."' LIMIT 1");
                        $pis = mysqli_fetch_object($piqry);
                          ?>
                        <div class="simpleLens-big-image-container"><a data-lens-image="<?php echo UPLOAD_PATH_DISPLAY.$pis->product_image; ?>" class="simpleLens-lens-image"><img src="<?php echo UPLOAD_PATH_DISPLAY.$pis->product_image; ?>" style="width: 100%;" class="simpleLens-big-image"></a></div>
                      </div>
                        <br/>
                      <div class="simpleLens-thumbnails-container">
                          <?php 
                           $piqry = mysqli_query($con, "SELECT image_name as product_image FROM product_images WHERE product_id = '".$r->id."'");
                            while($pi = mysqli_fetch_object($piqry)){
                          ?>
                          <a data-big-image="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" data-lens-image="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" class="simpleLens-thumbnail-wrapper" href="#">
                              <img src="<?php echo UPLOAD_PATH_DISPLAY.$pi->product_image; ?>" class="" style="height: 80px;" />
                          </a>                                    
                          <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal view content -->
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="aa-product-view-content">
                      <form method="post" action="update-cart.php">
                          <input type="hidden" name="product_id" value="<?php echo $r->id; ?>"/>
                        <input type="hidden" name="product_image" value="<?php echo $pis->product_image; ?>"/>
                        <input type="hidden" name="brand_id" value="<?php echo $r->boutique_id; ?>"/>
                        <input type="hidden" name="type" value="add"/>
                        <input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
                            <h3><?php echo $r->name; ?></h3>
                            <div class="aa-price-block">
                              <span class="aa-product-view-price">PKR <?php echo $r->price; ?></span>
                              <p class="aa-product-avilability">Availability: <?php if($r->stock != "" && $r->stock <= 0){ ?><span style="color: red; font-weight: bold;">Out of stock</span><?php }else{ ?> <span style="color: green; font-weight: bold;">In stock (<?php echo $r->stock; ?>)</span><?php } ?></p>
                            </div>
                            <h4>Size</h4>
                            <div class="aa-prod-view-size">
                                <?php  
                                
                                    $PSsql = "SELECT s.* FROM `product_sizes` as ps JOIN sizes as s ON s.id = ps.size_id WHERE product_id = '".$r->id."'";
                                    $PSqry = mysqli_query($con, $PSsql);
                                ?>
                                <select name="size" class="form-control" required="required">
                                    <option value="">Select Size</option>
                                    <?php while($PSr = mysqli_fetch_object($PSqry)){ ?>
                                    <option value="<?php echo $PSr->short_name; ?>"><?php echo $PSr->short_name ; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <h4>Color</h4>
                            <div class="aa-color-tag">
                               <?php 
                                $colors = explode(",", $r->color);
                                ?>
                                <select name="color" class="form-control" required="required">
                                    <option value="">Select Color</option>
                                    <?php foreach($colors as $c=>$v){ ?>
                                    <option value="<?php echo $v; ?>"><?php echo $v ; ?></option>
                                    <?php } ?>
                                </select>                    
                            </div>
                            <div class="aa-prod-quantity">
                              <p class="aa-prod-category">
                                Quantity:
                                <input type="number" id="product_qty" name="product_qty" min="1" value="1" required="required" max="20"/>
                              </p>
                              
                              <p class="aa-prod-category">
                                Category: <a href="#"><?php echo $r->category_name; ?></a>
                              </p>
                            </div>
                            <div class="aa-prod-view-bottom">
                                <button type="submit" class="aa-add-to-cart-btn">Add To Cart</button>
                            </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="aa-product-details-bottom">
              <ul class="nav nav-tabs" id="myTab2">
                <li><a href="#description" data-toggle="tab">Description</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade in active" id="description">
                  <?php echo $r->description; ?>
                </div>          
              </div>
            </div>
            <!-- Related product -->
            <div class="aa-product-related-item">
              <h3>Related Products</h3>
              <ul class="aa-product-catg aa-related-item-slider">
                  <?php 
                            $pqry = mysqli_query($con, "SELECT p.*, pi.image_name as product_image FROM products as p "
                                                        . "LEFT JOIN product_images as pi ON p.id = pi.product_id "
                                                        . "ORDER BY rand() LIMIT 3");
                            
                            while($p = mysqli_fetch_object($pqry)){
                        ?>
                <!-- start single product item -->
                <li>
                    <form method="post" action="update-cart.php">
                            <input type="hidden" name="product_id" value="<?php echo $p->id; ?>"/>
                            <input type="hidden" name="product_image" value="<?php echo $p->product_image; ?>"/>
                            <input type="hidden" name="brand_id" value="<?php echo $p->boutique_id; ?>"/>
                            <input type="hidden" name="type" value="add"/>
                            <input type="hidden" name="product_qty" value="1"/>
                            <input type="hidden" name="return_url" value="<?php echo $current_url; ?>">
                  <figure>
                    <a class="aa-product-img" href="#">
                        <img src="<?php echo UPLOAD_PATH_DISPLAY.$p->product_image ; ?>" alt="<?php echo $p->name ; ?>"></a>
                    <?php if(array_search($p->id, array_column($_SESSION["cart_products"], 'product_id')) === false){ ?>
                                  <a class="aa-add-card-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                                    <?php }else{ ?>
                                   <a class="aa-add-card-btn dont-add"><span class="fa fa-shopping-cart"></span>Already Added</a>
                                  <?php } ?>
                              <figcaption>
                              <h4 class="aa-product-title"><a href="product-detail.php?id=<?php echo $p->id; ?>"><?php echo $p->name; ?></a></h4>
                              <span class="aa-product-price">Rs.<?php echo $p->price; ?></span>
                            </figcaption>
                  </figure>                   
                  <!-- product badge -->
                  <!--<span class="aa-badge aa-sale" href="#">SALE!</span>-->
                  </form>
                </li>
                <?php } ?>
                
              </ul>
               
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->

  <?php include("includes/footer.php"); ?>
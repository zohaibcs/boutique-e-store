<?php include("includes/header.php");  ?>
<?php 
    $oqry = mysqli_query($con, "SELECT COUNT(product_id) as pCount, o.status, o.id FROM orders as o "
            . "JOIN ordered_products as op ON op.order_id = o.id "
            . "WHERE o.customer_id = '".$_SESSION["id"]."' GROUP BY o.id") or die(mysqli_error($con));
?>

  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>My Account</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>                   
          <li class="active">My Account</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-3">
                  <ul class="nav nav-pills nav-stacked navbar-default">
                      <li class=""><a href="my-account.php">My Orders</a></li>
                  </ul>
              </div>
              <div class="col-md-9">
                  
                  <table class="table table-striped table-bordered table-responsive">
                      <thead>
                      <tr>
                          <th style="width: 50px">Sr #</th>                          
                          <th>Products</th>
                          <th style="width: 150px">Status</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php 
                      if(mysqli_num_rows($oqry)){
                      $no = 1;
                      while($r = mysqli_fetch_object($oqry)){ ?>
                      <tr>
                          <td><?php echo $no; ?></td>
                          <td class="text-center" style="font-weight: bold; color: red;"><?php echo $r->pCount ?></td>
                          <td>
                            <?php 
                                if($r->status == "Pending"){
                                    echo "<span class='statusBG bgYellow'>".$r->status."</span>"; 
                                }else if($r->status == "Canceled"){
                                    echo "<span class='statusBG bgRed'>".$r->status."</span>";
                                }else if($r->status == "Processing"){
                                    echo "<span class='statusBG bgOrange'>".$r->status."</span>";
                                }else{
                                    echo "<span class='statusBG bgGreen'>".$r->status."</span>";
                                }
                            ?>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="3">
                                
                              <?php
                            $Psql = "SELECT p.id as pid, p.name, p.price, op.* FROM ordered_products as op JOIN products as p ON p.id = op.product_id WHERE op.order_id = '".$r->id."'";
                                $sqlpqry = mysqli_query($con, $Psql) or die(mysqli_error($con));
                            while($pr = mysqli_fetch_object($sqlpqry)){
                                $piqry = mysqli_query($con, "SELECT image_name FROM product_images WHERE product_id = '".$pr->pid."' LIMIT 1");
                                $pi = mysqli_fetch_object($piqry);
                                ?>
                                <div class="row" style="font-size: 12px; ">
                                    <div class="col-sm-4">
                                        <label for="name" class="col-sm-5"> Product Name</label>
                                        <div class="col-sm-7">
                                            <?php echo $pr->name; ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-2">
                                        <div class="col-sm-12">
                                          <?php if(isset($pi->image_name)){ ?>
                                              <img src="<?php echo UPLOAD_PATH_DISPLAY.$pi->image_name ;?>" style="height: 50px;"/>
                                           <?php } ?>
                                        </div>
                                      </div>
                                    <div class="col-sm-6">
                                            <div class="col-sm-12 row">
                                                <label for="name" class="col-sm-1 "> Size</label>
                                                <div class="col-sm-1">
                                                    <?php echo $pr->size; ?>
                                                </div>
                                                <label for="name" class="col-sm-2"> Quantity</label>
                                                <div class="col-sm-1">
                                                    <?php echo $pr->quantity; ?>
                                                </div>
                                                <label for="name" class="col-sm-1 "> Color</label>
                                                <div class="col-sm-2">
                                                    <?php echo $pr->color; ?>
                                                </div>
                                                <label for="name" class="col-sm-2 control-label"> Price</label>
                                                <div class="col-sm-2">
                                                    <?php echo $pr->price; ?>
                                                </div>
                                            </div>
                                        
                                        <div class="col-sm-12 row">
                                                <label for="name" class="col-sm-2 control-label"> SubTotal</label>
                                                <div class="col-sm-2">
                                                    <?php echo $total += $pr->price * $pr->quantity; ?>
                                                </div>
                                        </div>
                                      </div>
                                  </div>
                              
                              <hr style="margin-top: 10px;margin-bottom: 10px;"/>
                            <?php } ?>
                              <div class="text-right" style="font-size: 12px; font-weight: bold;">Total: <?php echo $total." PKR"; ?></div>

                          </td>
                      </tr>
                      <?php $no++;}
                      }else{ ?>
                      <tr>
                          <td colspan="4">You did not place any order yet.</td>
                      </tr>
                      <?php } ?>
                      </tbody>
                  </table>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

  <?php include("includes/footer.php"); ?>